-- mysql dump 10.13  distrib 5.7.20, for linux (x86_64)
--
-- host: localhost    database: pasca_unisba
-- ------------------------------------------------------
-- server version	5.5.5-10.2.14-mariadb

/*!40101 set @old_character_set_client=@@character_set_client */;
/*!40101 set @old_character_set_results=@@character_set_results */;
/*!40101 set @old_collation_connection=@@collation_connection */;
/*!40101 set names utf8 */;
/*!40103 set @old_time_zone=@@time_zone */;
/*!40103 set time_zone='+00:00' */;
/*!40014 set @old_unique_checks=@@unique_checks, unique_checks=0 */;
/*!40014 set @old_foreign_key_checks=@@foreign_key_checks, foreign_key_checks=0 */;
/*!40101 set @old_sql_mode=@@sql_mode, sql_mode='no_auto_value_on_zero' */;
/*!40111 set @old_sql_notes=@@sql_notes, sql_notes=0 */;

--
-- table structure for table `banner`
--

drop table if exists `banner`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `banner` (
  `banner_id` int(11) not null auto_increment,
  `url` varchar(255) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`banner_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `banner`
--

lock tables `banner` write;
/*!40000 alter table `banner` disable keys */;
/*!40000 alter table `banner` enable keys */;
unlock tables;

--
-- table structure for table `contact`
--

drop table if exists `contact`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `contact` (
  `contact_id` int(11) not null auto_increment,
  `contact_name` varchar(255) default null,
  `contact_value` varchar(255) default null,
  primary key (`contact_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `contact`
--

lock tables `contact` write;
/*!40000 alter table `contact` disable keys */;
insert into `contact` values (1,'email','pascasarjana@unisba.ac.id'),(2,'telp','022 4205546 ext 148 - 149'),(3,'address','jl. purnawarman no 59, bandung'),(4,'facebook','https://www.facebook.com/pascasarjanaunisba'),(5,'twitter','https://twitter.com/pascaunisba'),(6,'copyright','© 2018 pascasarjana unisba. all rights reserved.');
/*!40000 alter table `contact` enable keys */;
unlock tables;

--
-- table structure for table `dosen`
--

drop table if exists `dosen`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `dosen` (
  `dosen_id` int(11) not null auto_increment,
  `jabatan_id` int(11) default null,
  `nama` varchar(255) default null,
  `img_url` varchar(255) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`dosen_id`),
  key `fk_relationship_1` (`jabatan_id`),
  constraint `fk_relationship_1` foreign key (`jabatan_id`) references `jabatan` (`jabatan_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `dosen`
--

lock tables `dosen` write;
/*!40000 alter table `dosen` disable keys */;
/*!40000 alter table `dosen` enable keys */;
unlock tables;

--
-- table structure for table `dosen_pengajar`
--

drop table if exists `dosen_pengajar`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `dosen_pengajar` (
  `program_id` int(11) not null,
  `dosen_id` int(11) not null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`program_id`,`dosen_id`),
  key `fk_relationship_7` (`dosen_id`),
  constraint `fk_relationship_6` foreign key (`program_id`) references `program_magister_doktor` (`program_id`),
  constraint `fk_relationship_7` foreign key (`dosen_id`) references `dosen` (`dosen_id`)
) engine=innodb default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `dosen_pengajar`
--

lock tables `dosen_pengajar` write;
/*!40000 alter table `dosen_pengajar` disable keys */;
/*!40000 alter table `dosen_pengajar` enable keys */;
unlock tables;

--
-- table structure for table `galeri`
--

drop table if exists `galeri`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `galeri` (
  `galeri_id` int(11) not null auto_increment,
  `galeri_url` varchar(255) default null,
  `galeri_detail` varchar(255) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`galeri_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `galeri`
--

lock tables `galeri` write;
/*!40000 alter table `galeri` disable keys */;
/*!40000 alter table `galeri` enable keys */;
unlock tables;

--
-- table structure for table `informasi`
--

drop table if exists `informasi`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `informasi` (
  `informasi_id` int(11) not null auto_increment,
  `tipe_id` int(11) default null,
  `judul` varchar(255) default null,
  `informasi` longtext default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`informasi_id`),
  key `fk_relationship_3` (`tipe_id`),
  constraint `fk_relationship_3` foreign key (`tipe_id`) references `informasi_tipe` (`tipe_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `informasi`
--

lock tables `informasi` write;
/*!40000 alter table `informasi` disable keys */;
/*!40000 alter table `informasi` enable keys */;
unlock tables;

--
-- table structure for table `informasi_images`
--

drop table if exists `informasi_images`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `informasi_images` (
  `image_id` int(11) not null auto_increment,
  `informasi_id` int(11) default null,
  `image_url` varchar(255) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`image_id`),
  key `fk_relationship_2` (`informasi_id`),
  constraint `fk_relationship_2` foreign key (`informasi_id`) references `informasi` (`informasi_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `informasi_images`
--

lock tables `informasi_images` write;
/*!40000 alter table `informasi_images` disable keys */;
/*!40000 alter table `informasi_images` enable keys */;
unlock tables;

--
-- table structure for table `informasi_tipe`
--

drop table if exists `informasi_tipe`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `informasi_tipe` (
  `tipe_id` int(11) not null auto_increment,
  `tipe` varchar(20) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`tipe_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `informasi_tipe`
--

lock tables `informasi_tipe` write;
/*!40000 alter table `informasi_tipe` disable keys */;
/*!40000 alter table `informasi_tipe` enable keys */;
unlock tables;

--
-- table structure for table `jabatan`
--

drop table if exists `jabatan`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `jabatan` (
  `jabatan_id` int(11) not null auto_increment,
  `jabatan` varchar(255) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`jabatan_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `jabatan`
--

lock tables `jabatan` write;
/*!40000 alter table `jabatan` disable keys */;
/*!40000 alter table `jabatan` enable keys */;
unlock tables;

--
-- table structure for table `kalender`
--

drop table if exists `kalender`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `kalender` (
  `kalender_id` int(11) not null auto_increment,
  `details` text default null,
  `kalender_url` varchar(255) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`kalender_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `kalender`
--

lock tables `kalender` write;
/*!40000 alter table `kalender` disable keys */;
/*!40000 alter table `kalender` enable keys */;
unlock tables;

--
-- table structure for table `kurikulum_magister_doktor`
--

drop table if exists `kurikulum_magister_doktor`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `kurikulum_magister_doktor` (
  `kurikulum_id` int(11) not null auto_increment,
  `program_tipe_id` int(11) default null,
  `detail` text default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`kurikulum_id`),
  key `fk_relationship_9` (`program_tipe_id`),
  constraint `fk_relationship_9` foreign key (`program_tipe_id`) references `program_tipe` (`program_tipe_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `kurikulum_magister_doktor`
--

lock tables `kurikulum_magister_doktor` write;
/*!40000 alter table `kurikulum_magister_doktor` disable keys */;
/*!40000 alter table `kurikulum_magister_doktor` enable keys */;
unlock tables;

--
-- table structure for table `page`
--

drop table if exists `page`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `page` (
  `page_id` int(11) not null auto_increment,
  `page_name` varchar(255) not null,
  `page_title` varchar(255) default null,
  `page_content` text default null,
  `created_time` timestamp null default '0000-00-00 00:00:00',
  `updated_time` timestamp null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`page_id`)
) engine=innodb auto_increment=5 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `page`
--

lock tables `page` write;
/*!40000 alter table `page` disable keys */;
insert into `page` values (1,'sejarah','sejarah pascasarjana','<h1 style=\"box-sizing: border-box; margin: 0px; font-size: 13px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">sejarah pascasarjana unisba</span></h1>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">berdasarkan keputusan direktur direktorat jenderal pendidikan tinggi republik indonesia nomor 235/dikti/kep/1998 tanggal 09 juli 1998, universitas islam bandung (unisba) diberi kepercayaan oleh pemerintah untuk menyelenggarakan program magister (s2) ilmu hukum. persaingan untuk memperoleh kesempatan studi tingkat magister di bidang ilmu hukum, nampak semakin ketat.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">sementara itu perguruan tinggi yang menyelenggarakan program tersebut, tergolong masih langka. atas dasar itu, kami hadir memberikan alternatif bagi anda yang telah berpendidikan s1 (semua jurusan), untuk melanjutkan studi ke program s2 bidang ilmu hukum dengan konsentrasi hukum islam dan hukum pidana. sementara itu mulai tahun akademik 2001/2002, dibuka konsentrasi hukum administrasi negara (han) dengan kekhususan otonomi daerah. di samping itu, berdasarkan keputusan. dirjen binbaga islam no.e/335/99 tanggal 20 oktober 1999, juga telah dibuka program magister ilmu agama islam dengan konsentrasi manajemen pendidikan islam. kini, telah dibuka juga program magister (s2) ilmu komunikasi dengan izin penyelenggara, nomor 2952/d/t/2003, tanggal 10 oktober 2003 dan saat ini telah mendapatkan akreditasi c. pada tahun 2006 telah dibuka program studi doktor ilmu hukum dengan nilai akreditasi b berdasarkan keputusan badan akreditasi nasional perguruan tinggi nomor :024/ban-pt/ak-x/s3/i/2012 dan program studi magister profesi psikologi dengan ijin &acirc; penyelenggaraan nomor :786t/2001 dan yang terakhir dibuka program magister ilmu komunikasi dengan konsentrasi komunikasi bisnis, komunikasi politik dan konsentrasi komunikasi dakwah.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">beberapa tahun kemudian lahir magister manajemen konsentrasi manajemen rumah sakit dan magister ilmu hukum konsentrasi hukum kesehatan.</p>','2018-06-05 17:00:00','2018-06-28 17:00:00',null,null),(2,'struktur_organisasi','struktur organisasi','<h1 style=\"box-sizing: border-box; margin: 0px; font-size: 13px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">struktur organisasi pascasarjana unisba1</span></h1>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><a style=\"box-sizing: border-box; background: transparent; color: #3e3e3e; text-decoration-line: none; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; transition: all 0.3s ease-in-out;\" href=\"http://pasca.unisba.ac.id/wp-content/uploads/struktur-organisasi.jpg\"><img class=\"alignnone wp-image-3275 size-large\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; font-family: inherit; font-weight: inherit; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; background: transparent; max-width: 100%; height: auto;\" src=\"http://pasca.unisba.ac.id/wp-content/uploads/struktur-organisasi-1024x388.jpg\" alt=\"\" width=\"1024\" height=\"388\" /></a></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">rektor1</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">prof. dr. h. edi setiadi, s.h., m.h.&nbsp;</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">direktur&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">prof. dr. h. dey ravena, s.h., m.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">asisten direktur&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">dr. oji kurniadi, m.si.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi doktor ilmu hukum&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">prof. dr. hj. neni sri imaniyati, sh.,mh.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister ilmu hukum&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. chepi ali firman zakaria, s.h., m.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister pendidikan islam&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">sobar al-ghazal ,drs., m.pd.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister ilmu komunikasi&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">dr. anne maryani, dra.,m.si.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister psikologi&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">dr. ihsana sabriani borualogi,&nbsp;m.si., psikolog</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister manajemen&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">prof. dr. muhardi, se., m.si.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister perencanaan wilayah dan kota&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. ernawati hendrakusumah, dra., msp.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi&nbsp;ekonomi syariah&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. neneng nurhasanah, dra., m.hum.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi&nbsp;magister kenoatariatan&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. hj. rini irianti sundary, s.h., m.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">kasi &nbsp;akademik&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">hilda ambarwati ,s.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">kasi adm. keuangan&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">betty ayu kurniawati</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">staf administrasi&nbsp;</span>:</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">&nbsp;kusmadi, s.pd.i</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">vina puji handayani, s.pd.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">adek saputra, s.e.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">lischa diana, siip</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">krisna karyasmara, a.md.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">yoda suryapringga, a.md</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">maya fatmawati</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">jaenal abidin</span></p>','2018-06-18 17:00:00','2018-06-27 02:17:54',null,null),(3,'tujuan','tujuan pascasarjana unisba','<h3 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">tujuan umum</span></h3>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666; text-align: justify;\">menghasilkan magister yang memiliki wawasan keislaman dan keilmuan yang komprehensif dan integratif dalam melahirkan pemikiran dan melakukan tindakan yang berguna bagi diri, masyarakat, lingkungan, agama, nusa dan bangsa yang didasari 3m (mujahid/pejuang, mujtahid/pemikir. dan mujaddid/pembaharu).</p>\r\n<hr style=\"box-sizing: content-box; height: 0px; margin-top: 20px; margin-bottom: 20px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #eeeeee; color: #666666; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px;\" />\r\n<h3 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">tujuan khusus</span></h3>\r\n<p>&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister ilmu hukum</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan insan akademis yang mampu menjalankan profesi sebagai dosen dan pemikir.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">pembela/penasehat hukum di bidang hukum publik dan privat.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli hukum yang inovatif untuk mengisi kebutuhan di instansi-instansi negara.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli hukum di corporat</li>\r\n</ol>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister pendidika</span><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">n islam</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan pendidik yang menguasai manajemen pendidikan.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">manajer pendidikan yang menguasai keislaman dan keilmuan.</li>\r\n</ol>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister ilmu komunikasi</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan insan yang ahli, cakap dan terampil dalam menjalankan aktivitas komunikasi di era globalisasi.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">menjadi pelaku komunikasi bisnis, komunikasi politik, dan komunikasi dakwah yang profesional.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli perancang, perencana, pengelola, dan peneliti sistem komunikasi yang berkualitas.</li>\r\n</ol>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister profesi psikologi</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">menghasilkan lulusan magister yang menguasai teori psikologi secara komprehensif untuk kepentingan pengembangan dan penerapannya dengan berpedoman pada nilai-nilai islam.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">adapun kualifikasi dan kompetensi lulusan yang ingin dicapai adalah :</li>\r\n</ol>\r\n<ul style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">mampu melakukan asesmen dan intervensi dalam bidang psikologi dengan berpedoman pada nilai-nilai islam.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">mampu melakukan penelitian psikologi secara komprehensif untuk kepentingan pengembangan dan penerapan di bidang profesi psikologi dengan berpedoman pada nilai-nilai islam.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">mampu menerapkan kode etik psikologi dalam aktivitas profesinya.</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister manajemen</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan insan yang ahli, cakap dan terampil dalam menjalankan aktivitas komunikasi di era globalisasi.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">menjadi pelaku komunikasi bisnis, komunikasi politik, dan komunikasi dakwah yang profesional.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli perancang, perencana, pengelola, dan peneliti sistem komunikasi yang berkualitas.</li>\r\n<li>&nbsp;</li>\r\n</ol>','2018-06-27 02:27:29','0000-00-00 00:00:00',null,null),(4,'kalender','kalender akademik','<h1 style=\"box-sizing: border-box; margin: 20px 0px 10px; font-size: 23px; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">kalender akademik</span></h1>\r\n<h3 style=\"box-sizing: border-box; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 5px; margin-bottom: 10px; font-size: 17px; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">pascasarjana</span></h3>\r\n<h3 style=\"box-sizing: border-box; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 5px; margin-bottom: 10px; font-size: 17px; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">2017/2018</span></h3>\r\n<hr style=\"box-sizing: content-box; height: 0px; margin-top: 20px; margin-bottom: 20px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #eeeeee; color: #333333; font-family: \'helvetica neue\', helvetica, arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\" />\r\n<h1 style=\"box-sizing: border-box; margin: 20px 0px 10px; font-size: 23px; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\"><a style=\"box-sizing: border-box; background: 0px 0px; color: #2a6496; outline: 0px;\" href=\"http://pasca.unisba.ac.id/wp-content/uploads/img_20180103_0001.jpg\"><img class=\"alignnone wp-image-3346 size-large\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; margin-left: auto; margin-right: auto;\" src=\"http://pasca.unisba.ac.id/wp-content/uploads/img_20180103_0001-728x1024.jpg\" alt=\"\" width=\"728\" height=\"1024\" /></a></span></h1>','2018-06-27 04:18:16','2018-06-27 04:26:58',null,null);
/*!40000 alter table `page` enable keys */;
unlock tables;

--
-- table structure for table `program_magister_doktor`
--

drop table if exists `program_magister_doktor`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `program_magister_doktor` (
  `program_id` int(11) not null auto_increment,
  `dosen_id` int(11) default null,
  `program_tipe_id` int(11) default null,
  `program_nama` varchar(255) default null,
  `konsentrasi` text default null,
  `alamat` text default null,
  `telepon` varchar(20) default null,
  `email` varchar(20) default null,
  `pendahuluan` text default null,
  `kompetensi_lulusan` text default null,
  `pengelola` text default null,
  `pmb` text default null,
  `kontak_person` text default null,
  `fasilitas` text default null,
  `proses_belajar` text default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`program_id`),
  key `fk_relationship_5` (`dosen_id`),
  key `fk_relationship_8` (`program_tipe_id`),
  constraint `fk_relationship_5` foreign key (`dosen_id`) references `dosen` (`dosen_id`),
  constraint `fk_relationship_8` foreign key (`program_tipe_id`) references `program_tipe` (`program_tipe_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `program_magister_doktor`
--

lock tables `program_magister_doktor` write;
/*!40000 alter table `program_magister_doktor` disable keys */;
/*!40000 alter table `program_magister_doktor` enable keys */;
unlock tables;

--
-- table structure for table `program_tipe`
--

drop table if exists `program_tipe`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `program_tipe` (
  `program_tipe_id` int(11) not null auto_increment,
  `program_nama` varchar(255) default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`program_tipe_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `program_tipe`
--

lock tables `program_tipe` write;
/*!40000 alter table `program_tipe` disable keys */;
/*!40000 alter table `program_tipe` enable keys */;
unlock tables;

--
-- table structure for table `sejarah`
--

drop table if exists `sejarah`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `sejarah` (
  `sejarah_id` int(11) not null auto_increment,
  `sejarah` text default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`sejarah_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `sejarah`
--

lock tables `sejarah` write;
/*!40000 alter table `sejarah` disable keys */;
/*!40000 alter table `sejarah` enable keys */;
unlock tables;

--
-- table structure for table `struktur_organisasi`
--

drop table if exists `struktur_organisasi`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `struktur_organisasi` (
  `struktur_id` int(11) not null auto_increment,
  `struktur_img` varchar(255) default null,
  `details` text default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_y` int(11) default null,
  primary key (`struktur_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `struktur_organisasi`
--

lock tables `struktur_organisasi` write;
/*!40000 alter table `struktur_organisasi` disable keys */;
/*!40000 alter table `struktur_organisasi` enable keys */;
unlock tables;

--
-- table structure for table `tujuan`
--

drop table if exists `tujuan`;
/*!40101 set @saved_cs_client     = @@character_set_client */;
/*!40101 set character_set_client = utf8 */;
create table `tujuan` (
  `tujuan_id` int(11) not null auto_increment,
  `tujuan_umum` text default null,
  `tujuan_khusus` text default null,
  `created_time` timestamp not null default current_timestamp() on update current_timestamp(),
  `updated_time` timestamp not null default '0000-00-00 00:00:00',
  `created_by` int(11) default null,
  `updated_by` int(11) default null,
  primary key (`tujuan_id`)
) engine=innodb auto_increment=7 default charset=latin1;
/*!40101 set character_set_client = @saved_cs_client */;

--
-- dumping data for table `tujuan`
--

lock tables `tujuan` write;
/*!40000 alter table `tujuan` disable keys */;
/*!40000 alter table `tujuan` enable keys */;
unlock tables;
/*!40103 set time_zone=@old_time_zone */;

/*!40101 set sql_mode=@old_sql_mode */;
/*!40014 set foreign_key_checks=@old_foreign_key_checks */;
/*!40014 set unique_checks=@old_unique_checks */;
/*!40101 set character_set_client=@old_character_set_client */;
/*!40101 set character_set_results=@old_character_set_results */;
/*!40101 set collation_connection=@old_collation_connection */;
/*!40111 set sql_notes=@old_sql_notes */;

-- dump completed on 2018-06-28  5:06:01
