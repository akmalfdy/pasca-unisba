/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     6/12/2018 1:21:28 PM                         */
/*==============================================================*/


drop table if exists BANNER;

drop table if exists DOSEN;

drop table if exists DOSEN_PENGAJAR;

drop table if exists GALERI;

drop table if exists INFORMASI;

drop table if exists INFORMASI_IMAGES;

drop table if exists INFORMASI_TIPE;

drop table if exists JABATAN;

drop table if exists KALENDER;

drop table if exists KURIKULUM_MAGISTER_DOKTOR;

drop table if exists PROGRAM_MAGISTER_DOKTOR;

drop table if exists PROGRAM_TIPE;

drop table if exists SEJARAH;

drop table if exists STRUKTUR_ORGANISASI;

drop table if exists TUJUAN;

drop table if exists VISI_MISI;

drop table if exists VISI_MISI_IMAGES;

/*==============================================================*/
/* Table: BANNER                                                */
/*==============================================================*/
create table BANNER
(
   BANNER_ID            int not null,
   URL                  varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (BANNER_ID)
);

/*==============================================================*/
/* Table: DOSEN                                                 */
/*==============================================================*/
create table DOSEN
(
   DOSEN_ID             int not null,
   JABATAN_ID           int,
   NAMA                 varchar(255),
   IMG_URL              varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (DOSEN_ID)
);

/*==============================================================*/
/* Table: DOSEN_PENGAJAR                                        */
/*==============================================================*/
create table DOSEN_PENGAJAR
(
   PROGRAM_ID           int not null,
   DOSEN_ID             int not null,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (PROGRAM_ID, DOSEN_ID)
);

/*==============================================================*/
/* Table: GALERI                                                */
/*==============================================================*/
create table GALERI
(
   GALERI_ID            int not null,
   GALERI_URL           varchar(255),
   GALERI_DETAIL        varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (GALERI_ID)
);

/*==============================================================*/
/* Table: INFORMASI                                             */
/*==============================================================*/
create table INFORMASI
(
   INFORMASI_ID         int not null,
   TIPE_ID              char(10),
   JUDUL                varchar(255),
   INFORMASI            longtext,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (INFORMASI_ID)
);

/*==============================================================*/
/* Table: INFORMASI_IMAGES                                      */
/*==============================================================*/
create table INFORMASI_IMAGES
(
   IMAGE_ID             char(10) not null,
   INFORMASI_ID         int,
   IMAGE_URL            varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (IMAGE_ID)
);

/*==============================================================*/
/* Table: INFORMASI_TIPE                                        */
/*==============================================================*/
create table INFORMASI_TIPE
(
   TIPE_ID              char(10) not null,
   TIPE                 varchar(20),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (TIPE_ID)
);

/*==============================================================*/
/* Table: JABATAN                                               */
/*==============================================================*/
create table JABATAN
(
   JABATAN_ID           int not null,
   JABATAN              varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (JABATAN_ID)
);

/*==============================================================*/
/* Table: KALENDER                                              */
/*==============================================================*/
create table KALENDER
(
   KALENDER_ID          int not null,
   DETAILS              text,
   KALENDER_URL         varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (KALENDER_ID)
);

/*==============================================================*/
/* Table: KURIKULUM_MAGISTER_DOKTOR                             */
/*==============================================================*/
create table KURIKULUM_MAGISTER_DOKTOR
(
   KURIKULUM_ID         int not null,
   PROGRAM_TIPE_ID      int,
   DETAIL               text,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (KURIKULUM_ID)
);

/*==============================================================*/
/* Table: PROGRAM_MAGISTER_DOKTOR                               */
/*==============================================================*/
create table PROGRAM_MAGISTER_DOKTOR
(
   PROGRAM_ID           int not null,
   DOSEN_ID             int,
   PROGRAM_TIPE_ID      int,
   PROGRAM_NAMA         varchar(255),
   KONSENTRASI          text,
   ALAMAT               text,
   TELEPON              varchar(20),
   EMAIL                varchar(20),
   PENDAHULUAN          text,
   KOMPETENSI_LULUSAN   text,
   PENGELOLA            text,
   PMB                  text,
   KONTAK_PERSON        text,
   FASILITAS            text,
   PROSES_BELAJAR       text,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (PROGRAM_ID)
);

/*==============================================================*/
/* Table: PROGRAM_TIPE                                          */
/*==============================================================*/
create table PROGRAM_TIPE
(
   PROGRAM_TIPE_ID      int not null,
   PROGRAM_NAMA         varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (PROGRAM_TIPE_ID)
);

/*==============================================================*/
/* Table: SEJARAH                                               */
/*==============================================================*/
create table SEJARAH
(
   SEJARAH_ID           char(10) not null,
   SEJARAH              text,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (SEJARAH_ID)
);

/*==============================================================*/
/* Table: STRUKTUR_ORGANISASI                                   */
/*==============================================================*/
create table STRUKTUR_ORGANISASI
(
   STRUKTUR_ID          int not null,
   STRUKTUR_IMG         varchar(255),
   DETAILS              text,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_Y            int,
   primary key (STRUKTUR_ID)
);

/*==============================================================*/
/* Table: TUJUAN                                                */
/*==============================================================*/
create table TUJUAN
(
   TUJUAN_ID            int not null,
   TUJUAN_UMUM          text,
   TUJUAN_KHUSUS        text,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (TUJUAN_ID)
);

/*==============================================================*/
/* Table: VISI_MISI                                             */
/*==============================================================*/
create table VISI_MISI
(
   VISI_MISI_ID         int not null,
   VISI_MISI            text,
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (VISI_MISI_ID)
);

/*==============================================================*/
/* Table: VISI_MISI_IMAGES                                      */
/*==============================================================*/
create table VISI_MISI_IMAGES
(
   VM_IMAGE_ID          int not null,
   VISI_MISI_ID         int,
   VM_IMAGE_URL         varchar(255),
   CREATED_TIME         timestamp,
   UPDATED_TIME         timestamp,
   CREATED_BY           int,
   UPDATED_BY           int,
   primary key (VM_IMAGE_ID)
);

alter table DOSEN add constraint FK_RELATIONSHIP_1 foreign key (JABATAN_ID)
      references JABATAN (JABATAN_ID) on delete restrict on update restrict;

alter table DOSEN_PENGAJAR add constraint FK_RELATIONSHIP_6 foreign key (PROGRAM_ID)
      references PROGRAM_MAGISTER_DOKTOR (PROGRAM_ID) on delete restrict on update restrict;

alter table DOSEN_PENGAJAR add constraint FK_RELATIONSHIP_7 foreign key (DOSEN_ID)
      references DOSEN (DOSEN_ID) on delete restrict on update restrict;

alter table INFORMASI add constraint FK_RELATIONSHIP_3 foreign key (TIPE_ID)
      references INFORMASI_TIPE (TIPE_ID) on delete restrict on update restrict;

alter table INFORMASI_IMAGES add constraint FK_RELATIONSHIP_2 foreign key (INFORMASI_ID)
      references INFORMASI (INFORMASI_ID) on delete restrict on update restrict;

alter table KURIKULUM_MAGISTER_DOKTOR add constraint FK_RELATIONSHIP_9 foreign key (PROGRAM_TIPE_ID)
      references PROGRAM_TIPE (PROGRAM_TIPE_ID) on delete restrict on update restrict;

alter table PROGRAM_MAGISTER_DOKTOR add constraint FK_RELATIONSHIP_5 foreign key (DOSEN_ID)
      references DOSEN (DOSEN_ID) on delete restrict on update restrict;

alter table PROGRAM_MAGISTER_DOKTOR add constraint FK_RELATIONSHIP_8 foreign key (PROGRAM_TIPE_ID)
      references PROGRAM_TIPE (PROGRAM_TIPE_ID) on delete restrict on update restrict;

alter table VISI_MISI_IMAGES add constraint FK_RELATIONSHIP_4 foreign key (VISI_MISI_ID)
      references VISI_MISI (VISI_MISI_ID) on delete restrict on update restrict;

