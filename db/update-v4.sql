-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: pasca_unisba_new
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biaya`
--

DROP TABLE IF EXISTS `biaya`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biaya` (
  `biaya_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `semester` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  PRIMARY KEY (`biaya_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biaya`
--

LOCK TABLES `biaya` WRITE;
/*!40000 ALTER TABLE `biaya` DISABLE KEYS */;
INSERT INTO `biaya` VALUES (1,9,1,7000000),(2,9,2,1000000),(3,11,1,5000000),(4,11,2,9000000),(5,10,1,10000000);
/*!40000 ALTER TABLE `biaya` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'email','pascasarjana@unisba.ac.id'),(2,'telp','022 4205546 ext 148 - 149'),(3,'address','jl. purnawarman no 59, bandung'),(4,'facebook','https://www.facebook.com/pascasarjanaunisba'),(5,'twitter','https://twitter.com/pascaunisba'),(6,'copyright','© 2018 pascasarjana unisba. all rights reserved.');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dosen`
--

DROP TABLE IF EXISTS `dosen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dosen` (
  `dosen_id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan_id` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`dosen_id`),
  KEY `fk_relationship_1` (`jabatan_id`),
  CONSTRAINT `fk_relationship_1` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`jabatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dosen`
--

LOCK TABLES `dosen` WRITE;
/*!40000 ALTER TABLE `dosen` DISABLE KEYS */;
INSERT INTO `dosen` VALUES (7,7,'Prof. Dr. H. Edi Setiadi, S.H., M.H.','4731a-rektor-216x300.jpg',1,'2018-06-29 23:21:59','2018-06-29 04:21:59',NULL,NULL),(8,8,'Prof. Dr. H. Dey Ravena, S.H., M.H.','886d9-photo-direktur-program2.png',2,'2018-06-29 23:22:08','2018-06-29 04:22:08',NULL,NULL),(9,9,'Prof. Dr. Hj. Neni Sri Imaniyati, S.H., M.H.','acca0-prof-neni.jpeg',3,'2018-06-29 04:23:45','0000-00-00 00:00:00',NULL,NULL),(10,10,'Chepi Ali Firman Zakaria, S.H., M.H.','ec6bb-chepi-s.h.-m.h..png',4,'2018-06-29 04:41:22','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `dosen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dosen_pengajar`
--

DROP TABLE IF EXISTS `dosen_pengajar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dosen_pengajar` (
  `program_id` int(11) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`program_id`,`dosen_id`),
  KEY `fk_relationship_7` (`dosen_id`),
  CONSTRAINT `fk_relationship_6` FOREIGN KEY (`program_id`) REFERENCES `program_magister_doktor` (`program_id`),
  CONSTRAINT `fk_relationship_7` FOREIGN KEY (`dosen_id`) REFERENCES `dosen` (`dosen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dosen_pengajar`
--

LOCK TABLES `dosen_pengajar` WRITE;
/*!40000 ALTER TABLE `dosen_pengajar` DISABLE KEYS */;
/*!40000 ALTER TABLE `dosen_pengajar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galeri`
--

DROP TABLE IF EXISTS `galeri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galeri` (
  `galeri_id` int(11) NOT NULL AUTO_INCREMENT,
  `galeri_url` varchar(255) DEFAULT NULL,
  `galeri_detail` varchar(255) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`galeri_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galeri`
--

LOCK TABLES `galeri` WRITE;
/*!40000 ALTER TABLE `galeri` DISABLE KEYS */;
/*!40000 ALTER TABLE `galeri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informasi`
--

DROP TABLE IF EXISTS `informasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informasi` (
  `informasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe_id` int(11) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `informasi` longtext DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`informasi_id`),
  KEY `fk_relationship_3` (`tipe_id`),
  CONSTRAINT `fk_relationship_3` FOREIGN KEY (`tipe_id`) REFERENCES `informasi_tipe` (`tipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informasi`
--

LOCK TABLES `informasi` WRITE;
/*!40000 ALTER TABLE `informasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `informasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informasi_images`
--

DROP TABLE IF EXISTS `informasi_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informasi_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `informasi_id` int(11) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  KEY `fk_relationship_2` (`informasi_id`),
  CONSTRAINT `fk_relationship_2` FOREIGN KEY (`informasi_id`) REFERENCES `informasi` (`informasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informasi_images`
--

LOCK TABLES `informasi_images` WRITE;
/*!40000 ALTER TABLE `informasi_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `informasi_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informasi_tipe`
--

DROP TABLE IF EXISTS `informasi_tipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informasi_tipe` (
  `tipe_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(20) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`tipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informasi_tipe`
--

LOCK TABLES `informasi_tipe` WRITE;
/*!40000 ALTER TABLE `informasi_tipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `informasi_tipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jabatan` (
  `jabatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(255) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jabatan`
--

LOCK TABLES `jabatan` WRITE;
/*!40000 ALTER TABLE `jabatan` DISABLE KEYS */;
INSERT INTO `jabatan` VALUES (7,'Rektor','2018-06-28 19:03:39','0000-00-00 00:00:00',NULL,NULL),(8,'Direktur Program Pascasarjana','2018-06-29 04:18:55','0000-00-00 00:00:00',NULL,NULL),(9,'Ketua Prodi Doktor Ilmu Hukum','2018-06-29 04:19:03','0000-00-00 00:00:00',NULL,NULL),(10,'Ketua Prodi Magister Ilmu Hukum','2018-06-29 04:40:24','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jurusan`
--

DROP TABLE IF EXISTS `jurusan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jurusan` (
  `jurusan_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `jurusan_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jurusan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jurusan`
--

LOCK TABLES `jurusan` WRITE;
/*!40000 ALTER TABLE `jurusan` DISABLE KEYS */;
INSERT INTO `jurusan` VALUES (1,9,'Hukum Islam'),(2,9,'Hukum Pidana');
/*!40000 ALTER TABLE `jurusan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kalender`
--

DROP TABLE IF EXISTS `kalender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kalender` (
  `kalender_id` int(11) NOT NULL AUTO_INCREMENT,
  `details` text DEFAULT NULL,
  `kalender_url` varchar(255) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`kalender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kalender`
--

LOCK TABLES `kalender` WRITE;
/*!40000 ALTER TABLE `kalender` DISABLE KEYS */;
/*!40000 ALTER TABLE `kalender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurikulum_magister_doktor`
--

DROP TABLE IF EXISTS `kurikulum_magister_doktor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurikulum_magister_doktor` (
  `kurikulum_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_tipe_id` int(11) DEFAULT NULL,
  `detail` text DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`kurikulum_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurikulum_magister_doktor`
--

LOCK TABLES `kurikulum_magister_doktor` WRITE;
/*!40000 ALTER TABLE `kurikulum_magister_doktor` DISABLE KEYS */;
INSERT INTO `kurikulum_magister_doktor` VALUES (9,9,'<h2 style=\"box-sizing: border-box; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 20px; margin-bottom: 10px; font-size: 20px; background-color: rgba(255, 255, 255, 0.7);\"><a style=\"box-sizing: border-box; background: 0px 0px; color: #2a6496; outline: 0px;\" href=\"http://pasca.unisba.ac.id/wp-content/uploads/Kurikulum-MH1.png\"><img class=\"wp-image-2991 aligncenter\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; margin-left: auto; margin-right: auto;\" src=\"http://pasca.unisba.ac.id/wp-content/uploads/Kurikulum-MH1.png\" alt=\"Kurikulum MH\" width=\"686\" height=\"935\" /></a></h2>','2018-06-29 18:31:02','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `kurikulum_magister_doktor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_content` text DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_time` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'sejarah','sejarah pascasarjana','<h1 style=\"box-sizing: border-box; margin: 0px; font-size: 13px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">sejarah pascasarjana unisba</span></h1>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">berdasarkan keputusan direktur direktorat jenderal pendidikan tinggi republik indonesia nomor 235/dikti/kep/1998 tanggal 09 juli 1998, universitas islam bandung (unisba) diberi kepercayaan oleh pemerintah untuk menyelenggarakan program magister (s2) ilmu hukum. persaingan untuk memperoleh kesempatan studi tingkat magister di bidang ilmu hukum, nampak semakin ketat.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">sementara itu perguruan tinggi yang menyelenggarakan program tersebut, tergolong masih langka. atas dasar itu, kami hadir memberikan alternatif bagi anda yang telah berpendidikan s1 (semua jurusan), untuk melanjutkan studi ke program s2 bidang ilmu hukum dengan konsentrasi hukum islam dan hukum pidana. sementara itu mulai tahun akademik 2001/2002, dibuka konsentrasi hukum administrasi negara (han) dengan kekhususan otonomi daerah. di samping itu, berdasarkan keputusan. dirjen binbaga islam no.e/335/99 tanggal 20 oktober 1999, juga telah dibuka program magister ilmu agama islam dengan konsentrasi manajemen pendidikan islam. kini, telah dibuka juga program magister (s2) ilmu komunikasi dengan izin penyelenggara, nomor 2952/d/t/2003, tanggal 10 oktober 2003 dan saat ini telah mendapatkan akreditasi c. pada tahun 2006 telah dibuka program studi doktor ilmu hukum dengan nilai akreditasi b berdasarkan keputusan badan akreditasi nasional perguruan tinggi nomor :024/ban-pt/ak-x/s3/i/2012 dan program studi magister profesi psikologi dengan ijin &acirc; penyelenggaraan nomor :786t/2001 dan yang terakhir dibuka program magister ilmu komunikasi dengan konsentrasi komunikasi bisnis, komunikasi politik dan konsentrasi komunikasi dakwah.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">beberapa tahun kemudian lahir magister manajemen konsentrasi manajemen rumah sakit dan magister ilmu hukum konsentrasi hukum kesehatan.</p>','2018-06-05 17:00:00','2018-06-28 17:00:00',NULL,NULL),(2,'struktur_organisasi','struktur organisasi','<h1 style=\"box-sizing: border-box; margin: 0px; font-size: 13px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">struktur organisasi pascasarjana unisba1</span></h1>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><a style=\"box-sizing: border-box; background: transparent; color: #3e3e3e; text-decoration-line: none; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; transition: all 0.3s ease-in-out;\" href=\"http://pasca.unisba.ac.id/wp-content/uploads/struktur-organisasi.jpg\"><img class=\"alignnone wp-image-3275 size-large\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; font-family: inherit; font-weight: inherit; font-style: inherit; margin: 0px; padding: 0px; outline: 0px; background: transparent; max-width: 100%; height: auto;\" src=\"http://pasca.unisba.ac.id/wp-content/uploads/struktur-organisasi-1024x388.jpg\" alt=\"\" width=\"1024\" height=\"388\" /></a></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">rektor1</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">prof. dr. h. edi setiadi, s.h., m.h.&nbsp;</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">direktur&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">prof. dr. h. dey ravena, s.h., m.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">asisten direktur&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">dr. oji kurniadi, m.si.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi doktor ilmu hukum&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">prof. dr. hj. neni sri imaniyati, sh.,mh.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister ilmu hukum&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. chepi ali firman zakaria, s.h., m.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister pendidikan islam&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">sobar al-ghazal ,drs., m.pd.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister ilmu komunikasi&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">dr. anne maryani, dra.,m.si.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister psikologi&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">dr. ihsana sabriani borualogi,&nbsp;m.si., psikolog</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister manajemen&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent; color: #808080;\">prof. dr. muhardi, se., m.si.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi magister perencanaan wilayah dan kota&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. ernawati hendrakusumah, dra., msp.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi&nbsp;ekonomi syariah&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. neneng nurhasanah, dra., m.hum.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ketua program studi&nbsp;magister kenoatariatan&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">dr. hj. rini irianti sundary, s.h., m.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">kasi &nbsp;akademik&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">hilda ambarwati ,s.h.</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">kasi adm. keuangan&nbsp;</span>:<br style=\"box-sizing: border-box;\" /><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">betty ayu kurniawati</span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">staf administrasi&nbsp;</span>:</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">&nbsp;kusmadi, s.pd.i</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">vina puji handayani, s.pd.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">adek saputra, s.e.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">lischa diana, siip</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">krisna karyasmara, a.md.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">yoda suryapringga, a.md</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">maya fatmawati</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\"><span style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">jaenal abidin</span></p>','2018-06-18 17:00:00','2018-06-27 02:17:54',NULL,NULL),(3,'tujuan','tujuan pascasarjana unisba','<h3 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">tujuan umum</span></h3>\r\n<p>&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666; text-align: justify;\">menghasilkan magister yang memiliki wawasan keislaman dan keilmuan yang komprehensif dan integratif dalam melahirkan pemikiran dan melakukan tindakan yang berguna bagi diri, masyarakat, lingkungan, agama, nusa dan bangsa yang didasari 3m (mujahid/pejuang, mujtahid/pemikir. dan mujaddid/pembaharu).</p>\r\n<hr style=\"box-sizing: content-box; height: 0px; margin-top: 20px; margin-bottom: 20px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #eeeeee; color: #666666; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px;\" />\r\n<h3 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; text-align: center;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">tujuan khusus</span></h3>\r\n<p>&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister ilmu hukum</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan insan akademis yang mampu menjalankan profesi sebagai dosen dan pemikir.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">pembela/penasehat hukum di bidang hukum publik dan privat.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli hukum yang inovatif untuk mengisi kebutuhan di instansi-instansi negara.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli hukum di corporat</li>\r\n</ol>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister pendidika</span><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">n islam</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan pendidik yang menguasai manajemen pendidikan.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">manajer pendidikan yang menguasai keislaman dan keilmuan.</li>\r\n</ol>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister ilmu komunikasi</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan insan yang ahli, cakap dan terampil dalam menjalankan aktivitas komunikasi di era globalisasi.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">menjadi pelaku komunikasi bisnis, komunikasi politik, dan komunikasi dakwah yang profesional.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli perancang, perencana, pengelola, dan peneliti sistem komunikasi yang berkualitas.</li>\r\n</ol>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister profesi psikologi</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">menghasilkan lulusan magister yang menguasai teori psikologi secara komprehensif untuk kepentingan pengembangan dan penerapannya dengan berpedoman pada nilai-nilai islam.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">adapun kualifikasi dan kompetensi lulusan yang ingin dicapai adalah :</li>\r\n</ol>\r\n<ul style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">mampu melakukan asesmen dan intervensi dalam bidang psikologi dengan berpedoman pada nilai-nilai islam.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">mampu melakukan penelitian psikologi secara komprehensif untuk kepentingan pengembangan dan penerapan di bidang profesi psikologi dengan berpedoman pada nilai-nilai islam.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">mampu menerapkan kode etik psikologi dalam aktivitas profesinya.</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #666666;\">&nbsp;</p>\r\n<h4 style=\"box-sizing: border-box; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-weight: inherit; line-height: 1.1; color: #666666; margin: 0px; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"box-sizing: border-box; font-weight: 900; font-family: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">program magister manajemen</span></h4>\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: \'palatino linotype\', \'book antiqua\', palatino, serif; font-size: 13px; vertical-align: baseline; padding: 0px 0px 0px 40px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">melahirkan insan yang ahli, cakap dan terampil dalam menjalankan aktivitas komunikasi di era globalisasi.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">menjadi pelaku komunikasi bisnis, komunikasi politik, dan komunikasi dakwah yang profesional.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">ahli perancang, perencana, pengelola, dan peneliti sistem komunikasi yang berkualitas.</li>\r\n<li>&nbsp;</li>\r\n</ol>','2018-06-27 02:27:29','0000-00-00 00:00:00',NULL,NULL),(4,'kalender','kalender akademik','<h1 style=\"box-sizing: border-box; margin: 20px 0px 10px; font-size: 23px; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">kalender akademik</span></h1>\r\n<h3 style=\"box-sizing: border-box; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 5px; margin-bottom: 10px; font-size: 17px; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">pascasarjana</span></h3>\r\n<h3 style=\"box-sizing: border-box; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; margin-top: 5px; margin-bottom: 10px; font-size: 17px; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">2017/2018</span></h3>\r\n<hr style=\"box-sizing: content-box; height: 0px; margin-top: 20px; margin-bottom: 20px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #eeeeee; color: #333333; font-family: \'helvetica neue\', helvetica, arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\" />\r\n<h1 style=\"box-sizing: border-box; margin: 20px 0px 10px; font-size: 23px; font-family: \'helvetica neue\', helvetica, arial, sans-serif; font-weight: 500; line-height: 1.1; color: #333333; text-align: center; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\"><a style=\"box-sizing: border-box; background: 0px 0px; color: #2a6496; outline: 0px;\" href=\"http://pasca.unisba.ac.id/wp-content/uploads/img_20180103_0001.jpg\"><img class=\"alignnone wp-image-3346 size-large\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; margin-left: auto; margin-right: auto;\" src=\"http://pasca.unisba.ac.id/wp-content/uploads/img_20180103_0001-728x1024.jpg\" alt=\"\" width=\"728\" height=\"1024\" /></a></span></h1>','2018-06-27 04:18:16','2018-06-27 04:26:58',NULL,NULL),(5,'visi_misi','Visi Misi','<p>-</p>','2018-06-29 18:58:54','2018-06-29 18:59:19',NULL,NULL);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pmb`
--

DROP TABLE IF EXISTS `pmb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pmb` (
  `pmb_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `konten` text DEFAULT NULL,
  PRIMARY KEY (`pmb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pmb`
--

LOCK TABLES `pmb` WRITE;
/*!40000 ALTER TABLE `pmb` DISABLE KEYS */;
INSERT INTO `pmb` VALUES (1,'syarat_pendaftaran','<div class=\"col-md-6\" style=\"box-sizing: border-box; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 13px; vertical-align: baseline; margin: 0px; padding: 0px 15px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; position: relative; min-height: 1px; float: left; width: 569px; color: #666666;\">\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background: transparent; list-style-position: initial; list-style-image: initial;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Berijazah S-1 semua Fakultas/Jurusan/Program Studi, kecuali untuk Magister Psikologi Profesi harus berijazah S-1 Psikolog</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Biaya Pendaftaran : Program Studi Magister Rp. 500.000,- khusus Program Studi Kenotariatan biaya formulir Rp 1.000.000,-</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Pas foto berwarna ukuran 3 x 4 dan 2 X 3 @ 3 lembar</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Foto copy Ijazah dan Transkrip Nilai S1 yang dilegalisasi, masing-masing 1 lembar.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Menyerahkan rekomendasi akademik</li>\r\n</ol>\r\n</div>\r\n<div class=\"col-md-6\" style=\"box-sizing: border-box; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 13px; vertical-align: baseline; margin: 0px; padding: 0px 15px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; position: relative; min-height: 1px; float: left; width: 569px; color: #666666;\">\r\n<ol style=\"box-sizing: border-box; margin: 0px; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background: transparent; list-style-position: initial; list-style-image: initial;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Biaya Pendaftaran : Rp 1.000.000,00</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Photo copy Ijasah dan Transkrip Nilai S1, dan S2 yang telah dilegalisasi masing-masing 1 lembar.</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Pas foto berwarna ukuran 2 x 3, 3 x 4 @ 3 lembar</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Photo copy Sertifikat Toefl skor minimal 450 (dibuktikan pada saat akan ujian kualifikasi)</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Makalah Ilmiah dalam bidang Ilmu hukum</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Rekomendasi Guru Besar</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Surat ijin belajar dari instansi asal, bagi yang sudah bekerja</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Rancangan proposal / usulan penelitian disertasi</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Pernah mengikuti Seminar, Simposium, dll</li>\r\n</ol>\r\n</div>'),(2,'jadwal','<table class=\"table table-bordered\" style=\"box-sizing: border-box; border-spacing: 0px; border-collapse: collapse; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 13px; vertical-align: baseline; margin: 0px 0px 20px; padding: 0px; border: 1px solid #dddddd; outline: 0px; width: 1108px; max-width: 100%; color: #666666;\">\r\n<tbody style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">\r\n<tr class=\"info\" style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">\r\n<th style=\"box-sizing: border-box; padding: 8px; text-align: left; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: #d9edf7; line-height: 1.42857;\">Kegiatan</th>\r\n<th style=\"box-sizing: border-box; padding: 8px; text-align: left; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: #d9edf7; line-height: 1.42857;\">Gelombang I</th>\r\n<th style=\"box-sizing: border-box; padding: 8px; text-align: left; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: #d9edf7; line-height: 1.42857;\">Gelombang II</th>\r\n</tr>\r\n<tr style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">Pendaftaran</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">2 Januari s.d. 29 Maret 2018</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">31 Maret s.d. 5 Juli 2018</td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">Seleksi</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">31 Maret 2018</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">7 Juli 2018</td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">Pengumuman Hasil Seleksi</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">4 April 2018</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">11 Juli 2018</td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">Registrasi</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">4 s.d. 18 April 2018</td>\r\n<td style=\"box-sizing: border-box; padding: 8px; font-family: inherit; font-size: 13px; font-weight: inherit; font-style: inherit; vertical-align: top; margin: 0px; border: 1px solid #dddddd; outline: 0px; background: transparent; line-height: 1.42857;\">11 s.d. 22 Juli 2018</td>\r\n</tr>\r\n</tbody>\r\n</table>'),(3,'materi','<ol style=\"box-sizing: border-box; margin: 0px 0px 0px 20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 13px; vertical-align: baseline; padding: 0px; border: 0px; outline: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; list-style-position: initial; list-style-image: initial; color: #666666;\">\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Wawancara</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Bahasa Inggris</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Tes Potensi Akademik (TPA)</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Psikotest (bagi calon Mahasiswa Magister Psikologi Profesi)</li>\r\n<li style=\"box-sizing: border-box; font-family: inherit; font-weight: inherit; font-style: inherit; vertical-align: baseline; margin: 0px; padding: 0px; border: 0px; outline: 0px; background: transparent;\">Keilmuan Hukum (bagi calon mahasiswa Prodi Magister Kenotariatan)</li>\r\n</ol>');
/*!40000 ALTER TABLE `pmb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `program_magister_doktor`
--

DROP TABLE IF EXISTS `program_magister_doktor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program_magister_doktor` (
  `program_id` int(11) NOT NULL AUTO_INCREMENT,
  `dosen_id` int(11) DEFAULT NULL,
  `program_tipe_id` int(11) DEFAULT NULL,
  `program_nama` varchar(255) DEFAULT NULL,
  `konsentrasi` text DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `telepon` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `pendahuluan` text DEFAULT NULL,
  `kompetensi_lulusan` text DEFAULT NULL,
  `pengelola` text DEFAULT NULL,
  `pmb` text DEFAULT NULL,
  `kontak_person` text DEFAULT NULL,
  `fasilitas` text DEFAULT NULL,
  `proses_belajar` text DEFAULT NULL,
  `tahapan_ujian` text DEFAULT NULL,
  `akreditasi` char(1) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`program_id`),
  KEY `fk_relationship_5` (`dosen_id`),
  CONSTRAINT `fk_relationship_5` FOREIGN KEY (`dosen_id`) REFERENCES `dosen` (`dosen_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `program_magister_doktor`
--

LOCK TABLES `program_magister_doktor` WRITE;
/*!40000 ALTER TABLE `program_magister_doktor` DISABLE KEYS */;
INSERT INTO `program_magister_doktor` VALUES (9,10,1,'Magister Ilmu Hukum','<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: helvetica;\">Konsentrasi Hukum Islam</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: helvetica;\">Konsentrasi Hukum Pidana</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: helvetica;\">Konsentrasi Hukum Administrasi Negara/OTDA</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: helvetica;\">Konsentrasi Hukum Bisnis</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: helvetica;\">Konsentrasi Hukum Kesehatan</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: helvetica;\">Konsentrasi Hukum Internasional</span></li>\r\n</ul>','<p><span style=\"box-sizing: border-box; font-weight: bold; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">Jl. Purnawarman No. 59&nbsp;</span></p>','022-4203368 Ext. 148','pascasarjana@unisba.','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7); text-align: justify;\"><span style=\"font-family: helvetica;\">Berdasarkan keputusan direktur Direktorat Jenderal Pendidikan tinggi republik indonesia nomor 235/Dikti/kep/1998 tanggal 09 Juli 1998. Universitas Islam Bandung menyelenggarakan program magister (S2) Ilmu hukum dengan konsentrasi hukum islam, hukum pidana, hukum administrasi negara dengan kekhususan otonomi daerah, dan hukum bisnis,</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7); text-align: justify;\"><span style=\"font-family: helvetica;\">Dokter dan dokter gigi sebagai profesi yang mulia, bebas dan penuh pengabdian. Namun demikian, saat ini mulai adanya pengaruh hukum yang cukup penting terhadap keberlangsungan keleluasaan profesi dokter dan dokter gigi memahami hukum, paling tidak mengetahui mana yang sesuai hukum dan mana yang melanggar hukum dalam menjalankan profesinya.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7); text-align: justify;\"><span style=\"font-family: helvetica;\">Berdasarkan hal itu, Tahun akademik 2006/2007 program studi ilmu hukum Pascasarjana UNISBA membuka konsentrasi hukum kesehatan.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7); text-align: justify;\"><span style=\"font-family: helvetica;\">Sehubungan dengan perkembangan dan kebutuhan masyarakat dan konsekuensi Indonesia sebagai anggota WTO, maka diperlukan ahli -ahli Hukum Internasional yang menguasai hukum internasional publik maupun privat maka tahun 2012 Pascasarjana Unisba membuka Konsentrasi Hukum Internasional.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7); text-align: justify;\"><span style=\"font-family: helvetica;\">Pada tahun 2015 telah Terakreditasi A-843/BAN-PT/Akred/M/VIII/2015.</span></p>','<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Melahirkan insan akademis yang mampu menjalankan profesi sebagai dosen dan pemikir di bidang ilmu hukum.</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Pembela/penasehat hukum dibidang hukum publik dan privat</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Ahli hukum yang inovatif untuk mengisi kebutuhan di instansi-instansi pemerintah atau swasta</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Ahli hukum di korporat dan ahli hukum independen</span></li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Minimal peserta program studi 20 orang atau per konsentrasi minimal 5 orang. Gelar akademik untuk program studi ilmu hukum adalah&nbsp;<span style=\"box-sizing: border-box; font-weight: bold;\">Magister Hukum&nbsp;</span>disingkat<span style=\"box-sizing: border-box; font-weight: bold;\">&nbsp;M.H. dan MH.Kes untuk Hukum Kesehatan.</span></span></p>\r\n<p><span style=\"box-sizing: border-box; font-weight: bold;\">&nbsp;</span></p>','<p>&nbsp;</p>\r\n<table style=\"border-collapse: collapse; width: 83.2099%; height: 79px;\" border=\"1\">\r\n<tbody>\r\n<tr style=\"height: 18px;\">\r\n<td style=\"width: 14.2928%; height: 18px;\"><span style=\"font-family: helvetica;\">Penanggung Jawab</span></td>\r\n<td style=\"width: 41.6412%; height: 18px;\"><span style=\"font-family: helvetica;\">(Prof. Dr.&nbsp;H. Edi Setiadi, S.H., M.H.)</span></td>\r\n</tr>\r\n<tr style=\"height: 18px;\">\r\n<td style=\"width: 14.2928%; height: 18px;\"><span style=\"font-family: helvetica;\">Direktur</span></td>\r\n<td style=\"width: 41.6412%; height: 18px;\"><span style=\"font-family: helvetica;\">Prof. Dr. H. Dey Ravena, S.H., M.H.</span></td>\r\n</tr>\r\n<tr style=\"height: 18px;\">\r\n<td style=\"width: 14.2928%; height: 18px;\"><span style=\"font-family: helvetica;\">Asisten Direktur</span></td>\r\n<td style=\"width: 41.6412%; height: 18px;\"><span style=\"font-family: helvetica;\">Dr. Oji Kurniadi, Drs., M.Si.</span></td>\r\n</tr>\r\n<tr style=\"height: 18px;\">\r\n<td style=\"width: 14.2928%; height: 18px;\"><span style=\"font-family: helvetica;\">Ketua Prodi</span></td>\r\n<td style=\"width: 41.6412%; height: 18px;\"><span style=\"font-family: helvetica;\">Dr. Chepi Ali Firman Zakaria, S.H., M.H.</span></td>\r\n</tr>\r\n</tbody>\r\n</table>','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">&nbsp;<a style=\"box-sizing: border-box; background: 0px 0px; color: #428bca; text-decoration-line: none;\" href=\"http://pasca.unisba.ac.id/wp-content/uploads/Landscape-2.jpg\"><img class=\"alignnone wp-image-3505\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; margin-left: auto; margin-right: auto;\" src=\"http://pasca.unisba.ac.id/wp-content/uploads/Landscape-2.jpg\" alt=\"\" width=\"800\" height=\"582\" /></a></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">Syarat Pendaftaran</span></p>\r\n<ol style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Berijazah S-1 semua Fakultas / Jurusan / Program Studi</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Mengisi/menyerahkan formulir pendaftaran</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan fotokopi ijazah S-1</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan transkrip nilai akademik S-1</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan pas foto berwarna terbaru ukuran 3&times;4 (3 lembar)</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan rekomendasi akademik dari dosen/pembimbing atau atasan calon mahasiswa.</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan makalah / proposal tentatif tesis minimal 2 atau 3 hlm.</span></li>\r\n</ol>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">PENDAFTARAN</span></p>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Waktu pendaftaran setiap hari kerja mulai jam 08.30 s.d 16.00</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Sekretariat pendaftaran di Jl. Purnawarman No. 59 Telp. 022-4203368 Ext 148-149. Fax. 4219134 Bandung 40116.</span></li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Biaya Formulir Pendafataran Rp. &nbsp;500.000</span><br style=\"box-sizing: border-box;\" /><span style=\"font-family: \'book antiqua\', palatino;\">Biaya pendidikan :</span></p>\r\n<table dir=\"ltr\" style=\"box-sizing: border-box; border-spacing: 0px; border-collapse: collapse; background-color: rgba(255, 255, 255, 0.7); color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\"><colgroup style=\"box-sizing: border-box;\"><col style=\"box-sizing: border-box;\" width=\"145\" /><col style=\"box-sizing: border-box;\" width=\"118\" /></colgroup>\r\n<tbody style=\"box-sizing: border-box;\">\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">SEMESTER</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">BIAYA</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER I</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp;10.000.000.</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER II</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; &nbsp;8.000.000.</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER III</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; &nbsp;8.000.000.</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER IV</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; &nbsp;6.000.000.</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">JUMLAH</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp;32.000.000.</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Materi Seleksi :</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">1. Bahasa Inggris</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">2. Tes Potensi Akademik/TPA</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">3. Wawancara</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;</span></p>','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Kontak Person :</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Maya Fatmawati&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 081223459378</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Vina Puji Handayani, S.Pd.&nbsp;: 082219212719</span></p>','<p><span style=\"color: #333333; font-family: \'book antiqua\', palatino; background-color: rgba(255, 255, 255, 0.7);\">Ruang kuliah ber-AC dengan system meeting, perpustakaan, internet. Teh dan Kopi disediakan.</span></p>','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Perkuliahan dengan sistem caturwulan dan proses pembimbingan bersifat progresif (terjadwal)</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Khusus untuk peserta rombongan minimal 20 peserta, jadwal dapat diatur oleh peserta dan hal lainnya dapat dibicarakan lebih lanjut.</span></p>','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Tahapan Ujian</span></p>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Seminar Proposal Tesis</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Seminar Hasil Penelitian</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Seminar Artikel</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Tesis</span></li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">KETENTUAN DAN PENJELASAN :</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\"><span style=\"box-sizing: border-box; font-weight: bold;\">&nbsp;1.&nbsp;</span>Seminar Hasil Penelitian :</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">2. Seminar hasil penelitian adalah seminar yang diselenggarakan oleh program studi bagi mahasiswa yang telah memperoleh data lapangan dan disusun dalam bentuk laporan sementara yang wajib diikuti sebelum sidang tesis atau sidang kelayakan naskah disertasi</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">3. Ketentuan seminar hasil penelitian :</span></p>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Mahasiswa menyerahkan bab I, III dan IV ke ketua Program Studi</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Dipresentasikan oleh mahasiswa dalam suatu forum dan ditelaah oleh 2 penelaah, ketua program studi dan pembimbing</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Diberikan penilaian layak/tidak layak untuk diuji dalam sidang lebih lanjut</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Mahasiswa harus menyelesaikan perbaikan naskah yang disetujui oleh pembimbing</span></li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">4. Seminar Artikel :</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">a. Seminar yang diselenggarakan oleh program studi bagi mahasiswa yang telah ujian naskah disertasi (sidang tertutup) bagi mahasiswa Program Doktor dan sesudah seminar hasil penelitian bagi Program Magister.</span></p>\r\n<ol style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\" start=\"2\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Ketentuan Seminar Artikel</span></li>\r\n</ol>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Mahasiswa menyerahkan artikel sebagai hasil tesis atau disertasi yang mengacu pada ketentuan jurnal internal unisba</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Sebelum seminar artikel, mahasiswa harus pernah menghadiri forum yang serupa minimal 3x</span></li>\r\n</ul>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Forum bersifat terbuka dan dihadiri oleh mahasiswa dan dipimpin oleh pembimbing</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Sebagai syarat untuk ujian sidang tesis atau disertasi</span></li>\r\n</ul>',NULL,NULL,'2018-07-03 14:53:35','2018-07-03 02:53:35',NULL,NULL),(10,9,2,'Doktor Ilmu Hukum','<p>-</p>',NULL,NULL,NULL,'<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7); text-align: justify;\"><span style=\"font-family: \'book antiqua\', palatino;\">Sejak tahun 1998 Program Pascasarjana Universitas Islam Bandung menyeleggarakan Program Magister (S2) Ilmu Hukum dengan Konsentrasi Hukum Islam, Hukum Pidana, Hukum Administrasi Negara/OTDA Hukum Bisnis dan Hukum Kesehatan, dan Terakreditasi SK BAN PT Nomor: 003/BAN-PT/Ak-III/S2/II/2005.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7); text-align: justify;\"><span style=\"font-family: \'book antiqua\', palatino;\">Alhamdulillah pada Tahun 2006 mendapat ijin dari Dirjen Dikti menyelenggarakan Program Doktor (S3) Ilmu Hukum dengan Nomor : 4925/D/T/2006, dan sekarang Terakreditasi B. SK BAN-PT.NO.024/BAN-PT/AK-X/S3/I/2012.</span></p>','<p>-</p>','<table dir=\"ltr\" style=\"box-sizing: border-box; border-spacing: 0px; border-collapse: collapse; background-color: rgba(255, 255, 255, 0.7); color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; height: 134px;\" border=\"1\" width=\"652\" cellspacing=\"0\" cellpadding=\"0\"><colgroup style=\"box-sizing: border-box;\"><col style=\"box-sizing: border-box;\" width=\"164\" /><col style=\"box-sizing: border-box;\" width=\"2\" /><col style=\"box-sizing: border-box;\" width=\"100\" /><col style=\"box-sizing: border-box;\" width=\"100\" /><col style=\"box-sizing: border-box;\" width=\"100\" /><col style=\"box-sizing: border-box;\" width=\"100\" /><col style=\"box-sizing: border-box;\" width=\"100\" /><col style=\"box-sizing: border-box;\" width=\"100\" /></colgroup>\r\n<tbody style=\"box-sizing: border-box;\">\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"2\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Penganggung Jawab&quot;]\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">Penanggung Jawab</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"6\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Rektor Universitas Islam Bandung Prof. Dr. dr., M. Thaufiq S. Boesoirie, M.S., Sp THT KL(K).&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">Rektor Universitas Islam Bandung&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (Prof. Dr.&nbsp;H. Edi Setiadi, S.H., M.H.)</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"2\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Direktur&quot;]\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">Direktur</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"6\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Prof. Dr. H. Dey Ravena, S.H., M.H.&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">Prof. Dr. H. Dey Ravena, S.H., M.H.</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"2\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Asisten Direktur&quot;]\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">Asisten Direktur</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"6\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Prof. Dr. Hj. Atie Rachmiatie, M.Si.&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">Dr. Oji Kurniadi, Drs., M.Si.</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"2\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Ketua Prodi&quot;]\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">Ketua Prodi</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" colspan=\"6\" rowspan=\"1\" data-sheets-value=\"[null,2,&quot;Dr. Hj. Neni Sri Imaniyati, S.H., M.H.&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">Prof. Dr. Hj. Neni Sri Imaniyati, S.H., M.H.</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;</span></p>','<hr style=\"box-sizing: content-box; height: 0px; margin-top: 20px; margin-bottom: 20px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #eeeeee; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\" />\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\"><a style=\"box-sizing: border-box; background: 0px 0px; color: #428bca; text-decoration-line: none;\" href=\"http://pasca.unisba.ac.id/wp-content/uploads/Landscape-2.jpg\"><img class=\"alignnone wp-image-3505\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; margin-left: auto; margin-right: auto;\" src=\"http://pasca.unisba.ac.id/wp-content/uploads/Landscape-2.jpg\" alt=\"\" width=\"800\" height=\"582\" /></a></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">SYARAT PENDAFTARAN :</span></p>\r\n<ol style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Syarat Akademik :</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Syarat Administratif :</span></li>\r\n</ol>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Berijazah Magister</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">IPK Magister minimal 3,0.</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Sertifikat Toefl skor minimal 450</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan makalah ilmiah dalam bidang ilmu hukum.</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Pernah mengikuti forum ilmiah</span><br style=\"box-sizing: border-box;\" /><span style=\"font-family: \'book antiqua\', palatino;\">(Seminar, workshop, simposium, dll. Dibuktikan dengan sertifikat).</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan rancangan Proposal Disertasi.</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Menyerahkan Daftar Riwayat Hidup</span></li>\r\n</ul>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Mengisi formulir pendaftaran</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Pas photo berwarna ukuran 2 x 3 dan 3 x 4 masing-masing 2 lembar</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Photo copy ijazah dan transkrip nilai S1 yang dilegalisasi, 1 lembar</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Photo copy ijazah dan transkrip nilai S2 yang dilegalisasi, 1 lembar</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Rekomendasi dari Guru Besar calon Promotor</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Surat ijin belajar dari instansi asal, bagi yang telah bekerja</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Persetujuan calon promotor</span></li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">SELEKSI :</span></p>\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Seleksi dilakukan berdasarkan persyaratan akademik, persyaratan administratif, penilaian karya ilmiah, rancangan proposal disertasi, dan wawancara.</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Bahasa Inggris (<em style=\"box-sizing: border-box;\">Reading Comprehension</em>)</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Test Potensi Akademik (TPA)</span></li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"box-sizing: border-box; font-weight: bold; font-family: \'book antiqua\', palatino;\">BIAYA :</span></p>\r\n<ol style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\">\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Biaya Pendaftaran Rp. 1.000.000,-</span></li>\r\n<li style=\"box-sizing: border-box;\"><span style=\"font-family: \'book antiqua\', palatino;\">Biaya Pendidikan selama enam semester (3 tahun) sebagai berikut :</span></li>\r\n</ol>\r\n<table dir=\"ltr\" style=\"box-sizing: border-box; border-spacing: 0px; border-collapse: collapse; background-color: rgba(255, 255, 255, 0.7); color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; height: 168px;\" border=\"1\" width=\"260\" cellspacing=\"0\" cellpadding=\"0\"><colgroup style=\"box-sizing: border-box;\"><col style=\"box-sizing: border-box;\" width=\"100\" /><col style=\"box-sizing: border-box;\" width=\"100\" /></colgroup>\r\n<tbody style=\"box-sizing: border-box;\">\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px; text-align: center;\" data-sheets-value=\"[null,2,&quot;SEMESTER&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px; text-align: center;\" data-sheets-value=\"[null,2,&quot;BIAYA&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">BIAYA</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,&quot;SEMESTER   I&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER I</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,3,null,17000000]\" data-sheets-numberformat=\"[null,2,&quot;#,###&quot;,1]\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; 20.000.000</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,&quot;SEMESTER   II&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER II</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,3,null,17000000]\" data-sheets-numberformat=\"[null,2,&quot;#,###&quot;,1]\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; 20.000.000</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,&quot;SEMESTER   III&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER III</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,3,null,17000000]\" data-sheets-numberformat=\"[null,2,&quot;#,###&quot;,1]\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; 20.000.000</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,&quot;SEMESTER   IV&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER IV</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,3,null,17000000]\" data-sheets-numberformat=\"[null,2,&quot;#,###&quot;,1]\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; 20.000.000</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,&quot;SEMESTER   V&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER V</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,3,null,15000000]\" data-sheets-numberformat=\"[null,2,&quot;#,###&quot;,1]\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; 15.000.000</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,&quot;SEMESTER   VI&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">SEMESTER VI</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,3,null,15000000]\" data-sheets-numberformat=\"[null,2,&quot;#,###&quot;,1]\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp &nbsp; 15.000.000</span></td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,2,&quot;JUMLAH&quot;]\"><span style=\"font-family: \'book antiqua\', palatino;\">JUMLAH</span></td>\r\n<td style=\"box-sizing: border-box; padding: 0px;\" data-sheets-value=\"[null,3,null,98000000]\" data-sheets-numberformat=\"[null,2,&quot;#,###&quot;,1]\"><span style=\"font-family: \'book antiqua\', palatino;\">&nbsp;Rp 110.000.000</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Biaya pendidikan selama 6 semester (3 tahun) Rp 110.500.000 (Biaya tersebut sudah mencakup DPP Smt I sd. IV, UPU, Matrikulasi, dan semua tahapan ujian, bimbingan disertasi kecuali Sidang Promosi Doktor)</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Apabila melebihi 6 semester dikenakan biaya herregistrasi</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Kuliah dapat dilaksanakan apabila dalam satu kelas minimal diikuti 10 peserta.</span></p>','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Maya Fatmawati&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 081223459378</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: rgba(255, 255, 255, 0.7);\"><span style=\"font-family: \'book antiqua\', palatino;\">Vina Puji Handayani, S.Pd.&nbsp;: 082219212719</span></p>','<p>-</p>','<p>-</p>','<p>-</p>',NULL,NULL,'2018-06-30 01:20:20','0000-00-00 00:00:00',NULL,NULL),(11,8,1,'Magister Ilmu Pendidikan Islam',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'A','<div>SK BAN-PT No :3223/SK/BAN-PT/Akred/PSPP/IX/2017</div>','2018-07-03 14:53:57','2018-07-03 02:53:57',NULL,NULL);
/*!40000 ALTER TABLE `program_magister_doktor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `program_tipe`
--

DROP TABLE IF EXISTS `program_tipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program_tipe` (
  `program_tipe_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_nama` varchar(255) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`program_tipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `program_tipe`
--

LOCK TABLES `program_tipe` WRITE;
/*!40000 ALTER TABLE `program_tipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `program_tipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sejarah`
--

DROP TABLE IF EXISTS `sejarah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sejarah` (
  `sejarah_id` int(11) NOT NULL AUTO_INCREMENT,
  `sejarah` text DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`sejarah_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sejarah`
--

LOCK TABLES `sejarah` WRITE;
/*!40000 ALTER TABLE `sejarah` DISABLE KEYS */;
/*!40000 ALTER TABLE `sejarah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `struktur_organisasi`
--

DROP TABLE IF EXISTS `struktur_organisasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `struktur_organisasi` (
  `struktur_id` int(11) NOT NULL AUTO_INCREMENT,
  `struktur_img` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_y` int(11) DEFAULT NULL,
  PRIMARY KEY (`struktur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `struktur_organisasi`
--

LOCK TABLES `struktur_organisasi` WRITE;
/*!40000 ALTER TABLE `struktur_organisasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `struktur_organisasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tujuan`
--

DROP TABLE IF EXISTS `tujuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tujuan` (
  `tujuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `tujuan_umum` text DEFAULT NULL,
  `tujuan_khusus` text DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`tujuan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tujuan`
--

LOCK TABLES `tujuan` WRITE;
/*!40000 ALTER TABLE `tujuan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tujuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','$2y$12$OyaknZ0Ahp8p93q.5hIOxeBgIcXxFKJD4os4SUCzisXvcamfreGZK');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-04  7:13:20
