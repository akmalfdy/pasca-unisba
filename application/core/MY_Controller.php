<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $data = Array(); //protected variables goes here its declaration

    function __construct() {

       parent::__construct();
    }

}

class CMS_Controller extends CI_Controller {

    function __construct() {

        parent::__construct();
        
        $this->load->library('session');
        date_default_timezone_set('Asia/Jakarta');
        
        if ($this->session->userdata('who') == '') {
            redirect('/Auth/login');
            
            die();
        }
    }

}
