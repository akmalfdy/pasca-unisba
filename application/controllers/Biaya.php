<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biaya extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'Contact_model', 'Program_model', 'Kurikulum_model', 'Biaya_model'
		]);
	}
	
	public function index()
	{
		$magister = [];
		$total = [];
		$doktor = [];
		foreach($this->Biaya_model->biayaMagister() as $row){
			$magister[$row->semester][] = $row;
		}
		foreach($this->Biaya_model->biayaKonsentrasi() as $row){
			$magister[$row->semester][] = $row;
		}
		
		foreach($this->Biaya_model->biayaDoktor() as $row){
			$doktor[$row->semester][] = $row;
		}
		
		$data['biayaMagister'] = $magister;
		$data['biayaDoktor'] = $doktor;
		$data['biayaMagisterKet'] = $this->Biaya_model->biayaKeterangan(1);
		$data['biayaDoktorKet'] = $this->Biaya_model->biayaKeterangan(2);
		
		$data['address'] = $this->Contact_model->item('address')->contact_value;
		$data['magister'] = $this->Program_model->getMagister();
		$data['doktor'] = $this->Program_model->getDoktor();
		$data['email'] = $this->Contact_model->item('email')->contact_value;
		$data['telp'] = $this->Contact_model->item('telp')->contact_value;
		$data['facebook'] = $this->Contact_model->item('facebook')->contact_value;
		$data['twitter'] = $this->Contact_model->item('twitter')->contact_value;
		$data['copyright'] = $this->Contact_model->item('copyright')->contact_value;
		
		$this->load->view('frontend/include/header', $data);
		$this->load->view('frontend/biaya_kuliah', $data);
		$this->load->view('frontend/include/footer', $data);
	}

}
