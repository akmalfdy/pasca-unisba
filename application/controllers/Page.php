<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'Page_model', 'Kurikulum_model', 'Member_model', 'Program_model', 'Kalender_model', 'Informasi_model', 'Banner_model', 'Galeri_model'
		]);
	}
	
	public function index($page)
	{
		$data = [];
		$view = 'biaya-kuliah';
		$page = str_replace("-","_",$page);
		
		switch ($page) {
			case 'home':
				$view = 'home';
				
				$data['galeri'] = $this->Galeri_model->all(10);
				$data['informasi'] = $this->Informasi_model->home();
				$data['banner'] = $this->Banner_model->all();
				
				break;
			case 'member':
				$data['dosen'] = $this->Member_model->getDosen();
				$view = 'member';
				break;
			case 'kalender':
				$data['kalender'] = $this->Kalender_model->get();
				$data['kurikulum'] = $this->Kurikulum_model->all();
				$data['informasi'] = $this->Informasi_model->home();

				$view = 'kalender';
				break;
			
			default:
				$view = 'page';
				break;
		}
		$data['page'] = $this->Page_model->page($page);
		
		view_front($view, $data);
	}

	public function home()
	{
		$this->index('home');
	}

	public function member()
	{
		$data['contact'] = $this->Contact_model->all();
		$data['about'] = $this->About_model->all();
		$data['brand'] = $this->Brand_model->all();
		$data['body'] = 'contact-us';

		$this->load->view('frontend/include/header', $data);
		$this->load->view('frontend/contact', $data);
		$this->load->view('frontend/include/footer', $data);
	}

	public function client()
	{
		$this->load->library('pagination');
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$config['base_url'] = base_url().'frontend/client';
		$config['per_page'] = 3;
		$config['total_rows'] = $this->Client_model->record_count($config["per_page"], $page);
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<div class="pagination-list">';
		$config['full_tag_close'] = '</div>';
		$config['num_tag_open'] = '<div class="page">';
		$config['num_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<div class="page active"><a>';
		$config['cur_tag_close'] = '</a></div>';
		$config['prev_tag_open'] = '<div class="page">';
		$config['prev_tag_close'] = '</div>';
		$config['next_tag_open'] = '<div class="page">';
		$config['next_tag_close'] = '</div>';
		$config['last_tag_open'] = '<div class="page">';
		$config['last_tag_close'] = '</div>';
		$config['first_tag_open'] = '<div class="page">';
		$config['first_tag_close'] = '</div>';
		$this->pagination->initialize($config);

		$data['contact'] = $this->Contact_model->all();
		$data['clients'] = $this->Client_model->fetch_client($config["per_page"], $page);
		$data['brand'] = $this->Brand_model->all();
		$data['about'] = $this->About_model->all();
		$data['body'] = 'portfolio-full masonry-top-title';
		$data['pagination'] = $this->pagination->create_links();

		$this->load->view('frontend/include/header', $data);
		$this->load->view('frontend/client', $data);
		$this->load->view('frontend/include/footer', $data);
	}

	public function product()
	{
		$this->load->library('pagination');
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$category = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$config['base_url'] = base_url().'frontend/product/'.$category;
		$config['per_page'] = 6;
		$config['total_rows'] = $this->Product_model->record_count($config["per_page"], $page, $category);
		$config["uri_segment"] = 4;
		$config['full_tag_open'] = '<div class="pagination-list">';
		$config['full_tag_close'] = '</div>';
		$config['num_tag_open'] = '<div class="page">';
		$config['num_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<div class="page active"><a>';
		$config['cur_tag_close'] = '</a></div>';
		$config['prev_tag_open'] = '<div class="page">';
		$config['prev_tag_close'] = '</div>';
		$config['next_tag_open'] = '<div class="page">';
		$config['next_tag_close'] = '</div>';
		$config['last_tag_open'] = '<div class="page">';
		$config['last_tag_close'] = '</div>';
		$config['first_tag_open'] = '<div class="page">';
		$config['first_tag_close'] = '</div>';
		$this->pagination->initialize($config);
		
		$data['contact'] = $this->Contact_model->all();
		$data['about'] = $this->About_model->all();
		$data['brand'] = $this->Brand_model->all();
		$data['products'] = $this->Product_model->fetch_product($config["per_page"], $page, $category);
		$data['product_category'] = $this->ProductCategory_model->all();
		$data['body'] = 'product-page product-grid product-3-columns-width-sidebar';

		$data['pagination'] = $this->pagination->create_links();

		$this->load->view('frontend/include/header', $data);
		$this->load->view('frontend/product-list', $data);
		$this->load->view('frontend/include/footer', $data);
	}

	public function productDetail($id)
	{
		$data['contact'] = $this->Contact_model->all();
		$data['about'] = $this->About_model->all();
		$data['brand'] = $this->Brand_model->all();
		$data['product'] = $this->Product_model->find($id);
		$data['product_related'] = $this->Product_model->related($data['product']->product_category_id);
		$data['body'] = 'product-page single-product affiliate-product';
		
		$this->load->view('frontend/include/header', $data);
		$this->load->view('frontend/product-detail', $data);
		$this->load->view('frontend/include/footer', $data);
	}

	public function about()
	{
		$data['contact'] = $this->Contact_model->all();
		$data['about'] = $this->About_model->all();
		$data['brand'] = $this->Brand_model->all();
		$data['body'] = 'about-us';

		$this->load->view('frontend/include/header', $data);
		$this->load->view('frontend/about', $data);
		$this->load->view('frontend/include/footer', $data);
	}
}
