<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "Brand";
        
        $crud = new grocery_CRUD();

        $crud->set_table('brand');

        $crud->set_subject($title);

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        // $crud->unset_add_fields('created', 'updated');
        // $crud->unset_edit_fields('created', 'updated');
        // $crud->unset_columns('created', 'avatar');
        // $crud->timestamps('created', 'updated');
        $crud->set_field_upload('brand_logo', 'assets/uploads/img/brand/');
        $crud->callback_after_upload(array($this,'example_callback_after_upload'));
        $crud->set_field_upload('brand_background', 'assets/uploads/img/brand/');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Brand";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('brand', $title, $data);
    }

    function example_callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');
        
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
        
        if($field_info->field_name == 'brand_background'){
            $this->image_moo->load($file_uploaded)->resize(1920,400)->save($file_uploaded,true);
        } else {
            // $this->image_moo->load($file_uploaded)->resize(200,300)->save($file_uploaded,true);
        }
        
        return true;
    }

}
