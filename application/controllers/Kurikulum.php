<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurikulum extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'Contact_model', 'Program_model', 'Kurikulum_model', 'Informasi_model'
		]);
	}
	
	public function index()
	{

		$data['kurikulum'] = $this->Kurikulum_model->all();
		$data['informasi'] = $this->Informasi_model->home(5);
		$data['address'] = $this->Contact_model->item('address')->contact_value;
		
		view_front('kurikulum', $data);
	}

}
