<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProductCategory extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "Product Category";
        
        $crud = new grocery_CRUD();

        $crud->set_table('product_category');

        $crud->set_subject($title);

        $crud->unset_export();
        $crud->unset_print();
        // $crud->unset_add_fields('created', 'updated');
        // $crud->unset_edit_fields('created', 'updated');
        // $crud->unset_columns('created', 'avatar');
        // $crud->timestamps('created', 'updated');
        // $crud->set_field_upload('avatar', 'assets/uploads/img/contact/');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Product Category";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('product_category', $title, $data);
    }

}
