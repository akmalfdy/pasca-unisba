<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Background extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "Home Background";
        
        $crud = new grocery_CRUD();

        $crud->set_table('home_background');

        $crud->set_subject($title);

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        // $crud->unset_add_fields('created', 'updated');
        // $crud->unset_edit_fields('created', 'updated');
        // $crud->unset_columns('created', 'avatar');
        // $crud->timestamps('created', 'updated');
        $crud->callback_before_upload(array($this,'example_callback_before_upload'));
        $crud->set_field_upload('background_image', 'assets/uploads/img/');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Home Background";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('brand', $title, $data);
    }

    function example_callback_before_upload($files_to_upload,$field_info)
    {
        $this->load->model('Brand_model');
        $background = $this->Brand_model->home();

        unlink('./assets/uploads/img/'.$background->background_image);
        
        return true;
    }

}
