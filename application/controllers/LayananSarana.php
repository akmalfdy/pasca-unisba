<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LayananSarana extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'LayananSarana_model', 'Program_model'
		]);
	}

	public function index()
	{
		$data = [];
		$segment = $this->uri->segment_array();
		$end = (int) end($segment);
		
		$this->load->library('pagination');
		$page = (is_int($end)) ? $end : 0;
		
		$config['base_url'] = base_url().'layananSarana/index';
		$config['per_page'] = 5;
		$config['total_rows'] = $this->LayananSarana_model->record_count($config["per_page"], $page);
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$data['layanan_sarana'] = $this->LayananSarana_model->fetch_client($config["per_page"], $page);
		$data['pagination'] = $this->pagination->create_links();
		
		view_front('layanan_sarana', $data);
	}
	
	public function detail($id)
	{	
		$data = [];

		$data['detail'] = $this->LayananSarana_model->find($id);
		$data['program'] = $this->Program_model->getByNama($tipe, $program);
		
		view_front('detail_layanan_sarana', $data);
	}

}
