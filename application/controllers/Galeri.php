<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'Galeri_model', 'Informasi_model'
		]);
	}
	
	public function index()
	{
		$galeri = [];
		foreach($this->Galeri_model->all() as $row){
			$galeri[$row->galeri_kategori_id][] = $row;
		}
		
		$data['kategori'] = $galeri;
		$data['informasi'] = $this->Informasi_model->home(5);
		
		view_front('galeri', $data);
	}

}
