<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pmb extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'Contact_model', 'Program_model', 'Kurikulum_model', 'Pmb_model', 'Biaya_model'
		]);
	}
	
	public function index()
	{
		$magister = [];
		$doktor = [];
		foreach($this->Biaya_model->biayaMagister() as $row){
			$magister[$row->semester][] = $row;
		}

		foreach($this->Biaya_model->biayaDoktor() as $row){
			$doktor[$row->semester][] = $row;
		}
		
		$data['biayaMagister'] = $magister;
		$data['biayaDoktor'] = $doktor;

		$data['pmb'] = $this->Pmb_model->all();
		
		view_front('pmb', $data);
	}

}
