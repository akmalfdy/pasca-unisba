<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GaleriKategori extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "Kategori Galeri";
        
        $crud = new grocery_CRUD();

        $crud->set_table('galeri_kategori');

        $crud->set_subject($title);

        $crud->display_as('galeri_kategori_nama','Nama Kategori');
        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Kategori Galeri";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('Kategori Galeri', $title, $data);
    }

}
