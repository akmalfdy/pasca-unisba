<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BiayaKet extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "biaya";
        
        $crud = new grocery_CRUD();

        $crud->set_table('biaya_ket');

        $crud->field_type("tipe_program_id",'dropdown',array( '1'=>'Magister', '2'=>'Doktor') );
        $crud->set_subject($title);
        $crud->display_as('tipe_program_id','Tipe Program');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();
        

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "biaya";
        $output->state_type = $crud->getState();
        
        $data['output'] = $output;
        
        load_view('biaya', $title, $data);
    }

}
