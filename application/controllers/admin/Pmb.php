<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pmb extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "pmb";
        
        $crud = new grocery_CRUD();

        $crud->set_table('pmb');

        $crud->set_subject($title);

        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "pmb";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('pmb', $title, $data);
    }

}
