<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "Kontak";
        
        $crud = new grocery_CRUD();

        $crud->set_table('contact');

        $crud->set_subject($title);

        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        
        $crud->display_as('contact_name','Item Kontak');
        $crud->display_as('contact_value','Isi Kontak');
        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Kontak";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('contact', $title, $data);
    }

}
