<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "Jurusan";
        
        $crud = new grocery_CRUD();

        $crud->set_table('jurusan');

        $crud->set_subject($title);

        $crud->set_relation('program_id', 'program_magister_doktor', 'program_nama');
        $crud->display_as('program_tipe_id','Program');
        $crud->display_as('jurusan_name','Nama Jurusan');
        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Jurusan";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('Jurusan', $title, $data);
    }

}
