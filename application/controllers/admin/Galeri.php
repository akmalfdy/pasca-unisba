<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "Galeri";
        
        $crud = new grocery_CRUD();

        $crud->set_table('galeri');

        $crud->set_subject($title);

        $crud->set_relation('galeri_kategori_id', 'galeri_kategori', 'galeri_kategori_nama');
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_add_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_edit_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_columns('created_by', 'updated_by');
        $crud->timestamps('created_time', 'updated_time');
        $crud->display_as('galeri_url','Gambar');
        $crud->display_as('galeri_judul','Judul');
        $crud->set_field_upload('galeri_url', 'assets/uploads/img/');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Galeri";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('Galeri', $title, $data);
    }

    function example_callback_before_upload($files_to_upload,$field_info)
    {
        $this->load->model('Brand_model');
        $background = $this->Brand_model->home();

        unlink('./assets/uploads/img/'.$background->background_image);
        
        return true;
    }

}
