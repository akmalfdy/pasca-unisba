<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "Dosen";
        
        $crud = new grocery_CRUD();

        $crud->set_table('dosen');

        $crud->set_subject($title);

        $crud->set_relation('jabatan_id', 'jabatan', 'jabatan');
        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        $crud->display_as('jabatan_id','Jabatan');
        $crud->display_as('img_url','Foto');
        $crud->display_as('sequence','Urutan');
        $crud->display_as('created_time','Tanggal Dibuat');
        $crud->display_as('updated_time','Tanggal Update');
        $crud->unset_add_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_edit_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_columns('created_by', 'updated_by');
        $crud->timestamps('created_time', 'updated_time');
        $crud->callback_before_upload(array($this,'example_callback_before_upload'));
        $crud->set_field_upload('img_url', 'assets/uploads/img/');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Dosen";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('dosen', $title, $data);
    }

    function example_callback_before_upload($files_to_upload,$field_info)
    {
        // $this->load->model('Brand_model');
        // $background = $this->Brand_model->home();

        // unlink('./assets/uploads/img/'.$background->background_image);
        
        return true;
    }

}
