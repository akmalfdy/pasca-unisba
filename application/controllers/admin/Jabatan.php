<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "Jabatan";
        
        $crud = new grocery_CRUD();

        $crud->set_table('jabatan');

        $crud->set_subject($title);

        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        $crud->display_as('created_time','Tanggal Dibuat');
        $crud->display_as('updated_time','Tanggal Update');
        $crud->unset_add_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_edit_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_columns('created_by', 'updated_by');
        $crud->timestamps('created_time', 'updated_time');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Jabatan";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('jabatan', $title, $data);
    }

}
