<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "informasi";
        
        $crud = new grocery_CRUD();

        $crud->set_table('informasi');

        $crud->set_subject($title);

        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        $crud->display_as('image','Gambar');
        $crud->display_as('created_time','Tanggal Dibuat');
        $crud->display_as('updated_time','Tanggal Update');
        $crud->unset_add_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_edit_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_columns('created_by', 'updated_by');
        $crud->timestamps('created_time', 'updated_time');
        $crud->callback_before_upload(array($this,'example_callback_before_upload'));
        $crud->set_field_upload('image', 'assets/uploads/img/');
        $crud->set_field_upload('file', 'assets/uploads/img/');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "informasi";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('informasi', $title, $data);
    }

    function example_callback_before_upload($files_to_upload,$field_info)
    {
        // $this->load->model('Brand_model');
        // $background = $this->Brand_model->home();

        // unlink('./assets/uploads/img/'.$background->background_image);
        
        return true;
    }

}
