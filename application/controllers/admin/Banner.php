<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "Banner";
        
        $crud = new grocery_CRUD();

        $crud->set_table('banner');

        $crud->set_subject($title);

        // $crud->unset_add();
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_add_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_edit_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_columns('created_by', 'updated_by');
        $crud->timestamps('created_time', 'updated_time');
        $crud->display_as('url','Gambar');
        $crud->display_as('sequence','Urutan');
        $crud->set_field_upload('url', 'assets/uploads/img/');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "banner";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('banner', $title, $data);
    }

    function example_callback_before_upload($files_to_upload,$field_info)
    {
        $this->load->model('Brand_model');
        $background = $this->Brand_model->home();

        unlink('./assets/uploads/img/'.$background->background_image);
        
        return true;
    }

}
