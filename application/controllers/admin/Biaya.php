<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Biaya extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "biaya";
        
        $crud = new grocery_CRUD();

        $crud->set_table('biaya');

        $crud->field_type("tipe",'dropdown',array( '1'=>'Prodi', '2'=>'Konsentrasi') );
        $crud->set_subject($title);
        $crud->set_relation('program_id', 'program_magister_doktor', 'program_nama');
        $crud->unset_columns('tipe', 'updated_by');
        $crud->display_as('nama','Konsentrasi');
        $crud->display_as('program_id','Program');
        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();
        

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "biaya";
        $output->state_type = $crud->getState();
        
        $data['output'] = $output;
        
        load_view('biaya', $title, $data);
    }

}
