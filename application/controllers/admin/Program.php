<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "Program";
        
        $crud = new grocery_CRUD();

        $crud->set_table('program_magister_doktor');

        $crud->set_subject($title);

        $crud->set_relation('dosen_id', 'dosen', 'nama');
        // $crud->unset_add();
        // $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();
        $crud->display_as('dosen_id','Dosen');
        $crud->display_as('program_tipe_id','Tipe');
        $crud->display_as('program_nama','Nama Program');
        $crud->display_as('created_time','Tanggal Dibuat');
        $crud->display_as('updated_time','Tanggal Update');
        $crud->field_type("program_tipe_id",'dropdown',array( '1'=>'Magister', '2'=>'Doktor') );
        $crud->unset_add_fields('pengelola','pmb','kontak_person','created_time', 'updated_time', 'created_by', 'updated_by','akreditasi', 'keterangan');
        $crud->unset_edit_fields('pengelola','pmb','kontak_person','created_time', 'updated_time', 'created_by', 'updated_by','akreditasi', 'keterangan');
        $crud->columns('dosen_id', 'program_tipe_id', 'program_nama', 'created_time', 'updated_time');
        $crud->timestamps('created_time', 'updated_time');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Program";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('Program', $title, $data);
    }

    function example_callback_before_upload($files_to_upload,$field_info)
    {
        // $this->load->model('Brand_model');
        // $background = $this->Brand_model->home();

        // unlink('./assets/uploads/img/'.$background->background_image);
        
        return true;
    }

}
