<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
        $this->load->helper('file');
    }

    public function index() {
        $title = "Page";
        
        $crud = new grocery_CRUD();

        $crud->set_table('page');

        $crud->set_subject($title);

        $crud->unset_delete();
        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_export();
        $crud->unset_print();
        $crud->display_as('page_name','Item Page');
        $crud->display_as('page_title','Judul Page');
        $crud->display_as('page_content','Konten Page');
        $crud->display_as('created_time','Tanggal Dibuat');
        $crud->display_as('updated_time','Tanggal Update');
        $crud->unset_add_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_edit_fields('created_time', 'updated_time', 'created_by', 'updated_by');
        $crud->unset_columns('created_by', 'updated_by');
        $crud->timestamps('created_time', 'updated_time');

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Page";
        $data['output'] = $output;
        
        load_view('page', $title, $data);
    }

    function example_callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');
        
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
        
        if($field_info->field_name == 'brand_background'){
            $this->image_moo->load($file_uploaded)->resize(1920,400)->save($file_uploaded,true);
        } else {
            // $this->image_moo->load($file_uploaded)->resize(200,300)->save($file_uploaded,true);
        }
        
        return true;
    }

}
