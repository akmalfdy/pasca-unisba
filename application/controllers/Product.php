<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "Product";
        
        $crud = new grocery_CRUD();

        $crud->set_table('product');

        $crud->set_subject($title);

        $crud->unset_export();
        $crud->unset_print();
        $crud->set_relation('product_category_id', 'product_category', 'product_category_name');
        $crud->fields('product_name', 'product_category_id', 'product_image', 'product_desc');
        $crud->unset_add_fields('created_at', 'updated_at');
        $crud->unset_edit_fields('created_at', 'updated_at');
        // $crud->unset_columns('created', 'avatar');
        $crud->timestamps('created_at', 'updated_at');
        $crud->set_field_upload('product_image', 'assets/uploads/img/product/');
        $crud->callback_after_upload(array($this,'example_callback_after_upload'));

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Product";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('product', $title, $data);
    }

    function example_callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        $filename = pathinfo($uploader_response[0]->name, PATHINFO_FILENAME);
        $ext = pathinfo($uploader_response[0]->name, PATHINFO_EXTENSION );
        $file = $filename.'-300x400.'.$ext;
        
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        $dst = $field_info->upload_path.'/'.$file;
        
        $result = $this->image_moo->load($file_uploaded)->resize(300,400,TRUE)->save($dst,true);

        $file = $filename.'-350x420.'.$ext;
        $dst = $field_info->upload_path.'/'.$file;
        $result = $this->image_moo->load($file_uploaded)->resize(350,420,TRUE)->save($dst,true);
        
        return true;
    }

    public function shortcode(){
        $title = "Product Shortcode";
        
        $crud = new grocery_CRUD();

        $crud->set_table('product_shortcode');

        $crud->set_subject($title);

        $crud->unset_export();
        $crud->unset_print();
        $crud->set_relation_n_n('shortcodes', 'product_has_shortcode', 'product', 'product_shortcode_id', 'product_id', 'product_name', 'priority');
        // $crud->set_relation('product_category_id', 'product_category', 'product_category_name');
        // $crud->unset_add_fields('created_at', 'updated_at');
        // $crud->unset_edit_fields('created_at', 'updated_at');
        // $crud->unset_columns('created', 'avatar');
        // $crud->timestamps('created_at', 'updated_at');
        // $crud->set_field_upload('product_image', 'assets/uploads/img/product/');
        // $crud->callback_after_upload(array($this,'example_callback_after_upload'));
        

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Shortcode Product";
        // $output->state_type = $crud->getState();
        $data['output'] = $output;
        
        load_view('shortcode_product', $title, $data);
    }

 
}
