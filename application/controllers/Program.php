<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'program_model', 'Contact_model', 'Program_model', 'Informasi_model'
		]);
	}
	
	public function index()
	{
		$program = $this->uri->segment($this->uri->total_segments());
		$tipe = ($this->uri->segment($this->uri->total_segments() - 1) == 'magister') ? 1 : 2;
		
		$data = [];
		$program = str_replace("-"," ",$program);
		$data['program'] = $this->Program_model->getByNama($tipe, $program);
		$data['informasi'] = $this->Informasi_model->home(5);
		
		view_front('program', $data);
	}

}
