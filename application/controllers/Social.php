<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "Social";
        
        $crud = new grocery_CRUD();

        $crud->set_table('social');

        $crud->set_subject($title);

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_export();
        $crud->unset_print();

        $crud->field_type('social_status','dropdown', [0 => 'Inactive', 1 => 'Active']);

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Social";
        $data['output'] = $output;
        
        load_view('social', $title, $data);
    }

}
