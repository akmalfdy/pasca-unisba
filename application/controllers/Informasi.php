<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model([
			'Informasi_model', 'Program_model'
		]);
	}

	public function index()
	{
		$data = [];
		$segment = $this->uri->segment_array();
		$end = (int) end($segment);
		
		$this->load->library('pagination');
		$page = (is_int($end)) ? $end : 0;
		
		$config['base_url'] = base_url().'informasi/index';
		$config['per_page'] = 5;
		$config['total_rows'] = $this->Informasi_model->record_count($config["per_page"], $page);
		$config["uri_segment"] = 3;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$data['informasi'] = $this->Informasi_model->fetch_client($config["per_page"], $page);
		$data['pagination'] = $this->pagination->create_links();
		
		view_front('informasi', $data);
	}
	
	public function detail($id)
	{	
		$data = [];

		$data['detail'] = $this->Informasi_model->find($id);
		$data['program'] = $this->Program_model->getByNama($tipe, $program);
		$data['informasi'] = $this->Informasi_model->home();
		
		view_front('detail_informasi', $data);
	}

}
