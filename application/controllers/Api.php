<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function varian($product_category_id)
	{
		$this->load->model('Product_model');
		
		$data['products'] = $this->Product_model->fetch_product(5, 0, $product_category_id);

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}
}
