<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CMS_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD');
    }

    public function index() {
        $title = "CLient";
        
        $crud = new grocery_CRUD();

        $crud->set_table('client');

        $crud->set_subject($title);

        $crud->unset_export();
        $crud->unset_print();
        $crud->set_field_upload('client_image', 'assets/uploads/img/client/');
        $crud->callback_after_upload(array($this,'example_callback_after_upload'));

        $output = $crud->render();
        $output->subject = $title;
        $output->menu = "Client";
        $data['output'] = $output;
        
        load_view('client', $title, $data);
    }

    function example_callback_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        $filename = pathinfo($uploader_response[0]->name, PATHINFO_FILENAME);
        $ext = pathinfo($uploader_response[0]->name, PATHINFO_EXTENSION );
        $file = $filename.'-365x259.'.$ext;
        
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        $dst = $field_info->upload_path.'/'.$file;
        
        $result = $this->image_moo->load($file_uploaded)->resize(365,259,TRUE)->save($dst,true);
        
        return true;
    }

}
