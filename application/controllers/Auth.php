<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    protected $user_type_id;

    public function __construct() {
        parent::__construct();
        $this->load->library('session');

    }

    public function login() {
        if ($this->session->userdata('who')) {
            redirect('/admin/page');
            
            die();
        }

        $this->load->view('login.php');
    }

    public function do_login() {
        $username = $this->input->get_post('username', true);
        $password = $this->input->get_post('password', true);

        if ($username == '') {
            $this->session->set_flashdata('error', 'Username tidak boleh kosong');
        } else if ($password == '') {
            $this->session->set_flashdata('error', 'Password tidak boleh kosong');
        } else {
            $this->load->model('User_model');
            $user = $this->User_model->first();
            
            if (($user->username == $username) && password_verify($password, $user->password)) {
                $this->session->set_userdata('who', $user->username);
                $this->session->set_userdata('user', $user);
                
                redirect('/admin/page');
                die();
            } else {
                $this->session->set_flashdata('error', 'Username atau Password salah');
            }
        }

        redirect('/Auth/login');
    }

    public function logout() {        
        $this->session->unset_userdata('who');
        $this->session->sess_destroy();

        redirect('/Auth/login');
    }
}
