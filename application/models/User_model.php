<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model {
    
    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function first() {
        return $this->db->get('user')->row();
    }
}
