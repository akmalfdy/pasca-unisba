<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function page($page){
        $this->db->where('page_name', $page);

        return $this->db->get('page')->row();
    }
}
