<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class LayananSarana_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function all(){
        $this->db->order_by('created_time', 'DESC');

        return $this->db->get('layanan_sarana')->result();
    }

    public function find($id){
    	$this->db->where("layanan_sarana_id", $id);
        return $this->db->get('layanan_sarana')->row();
    }

    public function record_count($limit, $start) {
        $this->db->from("layanan_sarana");

        return $this->db->count_all_results();
    }

    public function fetch_client($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("layanan_sarana");
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}
