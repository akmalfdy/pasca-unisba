<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kurikulum_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function all(){
        $this->db->join('program_magister_doktor', 'program_magister_doktor.program_id = kurikulum_magister_doktor.program_tipe_id');
        $this->db->order_by('sequence', 'ASC');

        return $this->db->get('kurikulum_magister_doktor')->result();
    }
}
