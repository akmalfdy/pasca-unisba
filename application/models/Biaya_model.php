<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Biaya_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function biayaKonsentrasi(){
        $this->db->where('tipe', 2);

        return $this->db->get('biaya')->result();
    }

    public function biayaKeterangan($tipe){
        $this->db->where('tipe_program_id', $tipe);

        return $this->db->get('biaya_ket')->row();
    }

    public function biayaMagister(){
        $this->db->join('program_magister_doktor', 'program_magister_doktor.program_id = biaya.program_id and program_tipe_id = 1');

        return $this->db->get('biaya')->result();
    }

    public function biayaDoktor(){
        $this->db->join('program_magister_doktor', 'program_magister_doktor.program_id = biaya.program_id and program_tipe_id = 2');

        return $this->db->get('biaya')->result();
    }
}
