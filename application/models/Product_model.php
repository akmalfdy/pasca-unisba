<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function all(){
        return $this->db->get('product')->result();
    }

    public function related($id){
        $this->db->where('product_category_id', $id);
        $this->db->limit(4);
        
        return $this->db->get('product')->result();
    }

    public function find($id){
        $this->db->join('product_category', 'product_category.product_category_id = product.product_category_id');
        $this->db->where('product_id', $id);
        return $this->db->get('product')->row();
    }

    public function get_by_shortcode($id) {
        $this->db->select('product_has_shortcode.product_shortcode_id, product.*');
        $this->db->from('product_has_shortcode');
        $this->db->join('product', 'product.product_id = product_has_shortcode.product_id');
        $this->db->where('product_has_shortcode.product_shortcode_id', $id);
        
        return $this->db->get()->result();
    }

    public function record_count($limit, $start, $category) {
        if($category != 0) $this->db->where('product_category_id', $category);
        $this->db->from("product");

        return $this->db->count_all_results();
    }

    public function fetch_product($limit, $start, $category) {
        $this->db->limit($limit, $start);
        if($category != 0) $this->db->where('product_category_id', $category);
        $query = $this->db->get("product");
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}
