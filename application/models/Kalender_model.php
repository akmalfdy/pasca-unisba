<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kalender_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function get(){
        return $this->db->get('kalender')->row();
    }

}
