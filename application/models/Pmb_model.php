<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pmb_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function all(){

        return $this->db->get('pmb')->result();
    }
}
