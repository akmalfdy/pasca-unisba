<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getDosen(){
        $this->db->join('jabatan', 'jabatan.jabatan_id = dosen.jabatan_id');
    	$this->db->order_by("sequence","asc");
        return $this->db->get('dosen')->result();
    }

}
