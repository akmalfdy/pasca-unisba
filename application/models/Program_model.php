<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Program_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        error_reporting(E_ERROR);
    }

    public function getMagister(){
        $this->db->where('program_tipe_id', 1);
        $program = $this->db->get('program_magister_doktor')->result();

        foreach ($program as $key => $value) {
            $this->db->reset_query();
            $query = $this->db->where('program_id', $value->program_id);
            $value->jurusan = $this->db->get('jurusan')->result();
        }
        
        return $program;
    }

    public function getDoktor(){
        $this->db->where('program_tipe_id', 2);
        $program = $this->db->get('program_magister_doktor')->result();

        foreach ($program as $key => $value) {
            $this->db->where('program_id', $value->program_id);
            $value->jurusan = $this->db->get('jurusan')->result();
        }
        
        return $program;
    }

    public function getByNama($tipe, $name){
        $this->db->where('program_tipe_id', $tipe);
        $this->db->like('program_nama', $name);
        $this->db->join('dosen', 'dosen.dosen_id = program_magister_doktor.dosen_id');

        return $this->db->get('program_magister_doktor')->row();
    }
}
