<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informasi_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function home($limit = 4){
        $this->db->order_by('created_time', 'DESC');
        $this->db->limit($limit);

        return $this->db->get('informasi')->result();
    }

    public function find($id){
    	$this->db->where("informasi_id", $id);
        return $this->db->get('informasi')->row();
    }

    public function record_count($limit, $start) {
        $this->db->from("informasi");

        return $this->db->count_all_results();
    }

    public function fetch_client($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("informasi");
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}
