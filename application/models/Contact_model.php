<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function all(){
        return $this->db->get('contact')->result();
    }

    public function item($name){
        $this->db->where('contact_name', $name);

        return $this->db->get('contact')->row();
    }
}
