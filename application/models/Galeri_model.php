<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Galeri_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function all($limit = null){
        $this->db->join('galeri_kategori', 'galeri_kategori.galeri_kategori_id = galeri.galeri_kategori_id');
        if($limit) $this->db->limit($limit);

        return $this->db->get('galeri')->result();
    }
}
