<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function all(){
        $this->db->order_by('sequence', 'ASC');

        return $this->db->get('banner')->result();
    }
}
