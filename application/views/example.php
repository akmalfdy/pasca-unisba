
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left"><?= $output->menu ?: '' ; ?></h4>

                                    <!-- <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#"><?= $output->subject; ?></a></li>
                                        <li class="breadcrumb-item active"><?= $output->state_type; ?></li>
                                    </ol> -->

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><?php echo $output->subject ?></h4>

                                    <?php echo $output->output; ?>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    <?= $copyright ?>
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
            <script type="text/javascript">
                <?php if(isset($output->state_type)): ?>
                $(document).ready(function(){
                    <?php if($output->state_type == 'add'): ?>
                    $('#program_id_field_box').hide();
                    $('#nama_field_box').hide();
                    $('#field-tipe').change(function(){
                        if($(this).val() == 1){
                            $('#program_id_field_box').show();
                            $('#nama_field_box').hide();
                        } else {
                            $('#nama_field_box').show();
                            $('#program_id_field_box').hide();
                        }
                    });
                    <?php endif; ?>

                    <?php if($output->state_type == 'edit'): ?>                    
                    if($('#field-tipe').val() != 2){
                        $('#program_id_field_box').show();
                        $('#nama_field_box').hide();
                    } else {
                        $('#nama_field_box').show();
                        $('#program_id_field_box').hide();
                    }

                    $('#tipe_field_box').hide();
                    <?php endif; ?>
                });
                <?php endif; ?>
            </script>