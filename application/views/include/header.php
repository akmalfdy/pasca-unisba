<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Pasca Unisba</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Pasca Unisba" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>/assets/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>/assets/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>/assets/img/favicon-16x16.png">
        <link rel="manifest" href="<?= base_url(); ?>/assets/img/site.webmanifest">
        <link rel="mask-icon" href="<?= base_url(); ?>/assets/img/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <!-- App css -->
        <link href="<?= base_url(); ?>/assets/adminox/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.css" rel="stylesheet" type="text/css" /> -->
        <link href="<?= base_url(); ?>/assets/adminox/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>/assets/adminox/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url(); ?>/assets/adminox/assets/css/style.css" rel="stylesheet" type="text/css" />

        <link href="<?= base_url(); ?>/assets/adminox/plugins/jquery-toastr/jquery.toast.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
        <script src="<?= base_url(); ?>/assets/adminox/assets/js/modernizr.min.js"></script>

        <?php
        if (isset($output->css_files)) {
            foreach ($output->css_files as $file):
                ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
                <?php
            endforeach;
        }
        ?> 

        <?php
        if (isset($output->js_files)) {
            foreach ($output->js_files as $file):
                ?>
                <script src="<?php echo $file; ?>"></script>
                <?php
            endforeach;
        } else { ?>
             <script src="<?= base_url(); ?>/assets/adminox/assets/js/jquery.min.js"></script>
        <?php
        }
        ?>
    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left" style="padding: 5px;">
                    <a href="index.html" class="logo" style="line-height: 0px;">
                                <span>
                                    <img src="<?= base_url(); ?>/assets/university/images/logo-univ2.png" alt="" height="100%">
                                </span>
                        <i>
                            <img src="<?= base_url(); ?>/assets/university/images/logo-univ2.png" alt="" height="28">
                        </i>
                    </a>
                </div>

                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">

                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="<?= base_url(); ?>/assets/adminox/assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! Admin</small> </h5>
                                </div>

                                <!-- item-->
                                <a href="<?= site_url('/Auth/logout'); ?>" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        <li class="hide-phone app-search">
                            <!-- <form role="search" class="">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form> -->
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Navigasi</li>
                            <li>
                                <a href="<?= site_url('admin/banner'); ?>"><span> Banner </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/contact'); ?>"><span> Kontak </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/page'); ?>"><span> Page </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/jabatan'); ?>"><span> Jabatan </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/dosen'); ?>"><span> Dosen </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/program'); ?>"><span> Program </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/kurikulum'); ?>"><span> Kurikulum </span></a>
                            </li>
                            <li>
                                <a href="javascript: void(0);"><span> Biaya </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="<?= site_url('admin/biaya'); ?>">Biaya Kuliah</a></li>
                                    <li><a href="<?= site_url('admin/biayaKet'); ?>">Biaya Keterangan</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/pmb'); ?>"><span> PMB </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/kalender'); ?>"><span> Kalender </span></a>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/informasi'); ?>"><span> Informasi </span></a>
                            </li>
                            <li>
                                <a href="javascript: void(0);"><span> Galeri </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="<?= site_url('admin/galeriKategori'); ?>">Kategori</a></li>
                                    <li><a href="<?= site_url('admin/galeri'); ?>">Galeri</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?= site_url('admin/LayananSarana'); ?>"><span> Layanan dan Sarana </span></a>
                            </li>
                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



