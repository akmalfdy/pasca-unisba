<div id="preloaderKDZ"></div>
    <div class="sn-site">
    <header class="header sn-header-style-1">
        <div class="header-top">
          <div class="container">
            <div class="header-top-left">
              <aside id="text-2" class="widget widget_text">
                <div class="textwidget"><i class="fa fa-envelope-o"></i><?= $contact->email; ?></div>
              </aside>
              <aside id="text-3" class="widget widget_text">
                <div class="textwidget"><i class="fa fa-mobile"></i><?= $contact->phone; ?></div>
              </aside>
              <aside id="text-4" class="widget widget_text">
                <div class="textwidget"><i ></i></div>
              </aside>
            </div>
          </div>
        </div><a href="#primary-menu"><i class="fa fa-bars"></i></a>
        <div class="container">
          <div class="header-bottom">

            <div class="main-nav-wrapper header-left">
              <div class="header-logo pull-left"><a href="<?= base_url('/'); ?>" title="<?= $brand->brand_name; ?>"><img src="<?= base_url(); ?>/assets/uploads/img/brand/<?= $brand->brand_logo; ?>" style="height: 100px;" alt="logo" class="logo-img"/></a></div>
              <!-- .header-logo-->

              <nav id="primary-menu" class="main-nav">
                <ul class="nav">
                  <li class="active menu-single menu-home"><a href="<?= base_url('/'); ?>">Home</a>
                  <li class="menu-single"><a href="<?= site_url('page/product'); ?>">Product</a></li>
                  <li class="menu-single"><a href="<?= site_url('page/about'); ?>">About Us</a>
                  <li class="menu-single"><a href="<?= site_url('page/client'); ?>">Client</a>
                  <li class="menu-single"><a href="<?= site_url('page/contact'); ?>">Contact Us</a>
                  </li>
                </ul>
              </nav>
              <!-- .header-main-nav-->
            </div>
          </div>
        </div>
        
      </header>
      <div id="example-wrapper">
      <div class="section">
        <div class="banner-sub-page" style="background-image: url(<?= base_url(); ?>/assets/uploads/img/brand/<?= $brand->brand_background; ?>);">
            <div class="container">
              <div class="breadcrumbs">
                <div class="breadcrumbs-list">
                  <div class="page">Home</div>
                  <div class="page">About Us</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <section>
          <div class="container">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-10">
                <div class="about-us-content">
                  <h2><?= $brand->brand_name; ?></h2>
                  <div class="wpb_text_column wpb_content_element">
                    <div class="wpb_wrapper">
                      <?= $about->desc; ?>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </section>