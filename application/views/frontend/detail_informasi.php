<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title"><?= $detail->judul ?></h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="main-content blog-single">
    <div class="container">
        <div class="row">
            <div class="post-wrap">
                <div class="col-md-9">
                    <article class="post">
                        <div class="entry-wrapper">

                            <div class="post-content">
                                <h2 class="title"><?= $detail->judul ?></h2>
                                <p><?= $detail->informasi ?></p>
                                <?php if($detail->file): ?>
                                <?php $file = pathinfo(base_url('assets/uploads/img/'.$detail->file)); ?>
                                <a href="<?= base_url('assets/uploads/img/'.$detail->file)?>">Download <?= end(explode('/', $detail->file)) ?> <i class="fa <?= ($file['extension'] == 'pdf') ? 'fa-file-pdf-o' : 'fa-file-word-o' ?>"></i></a>
                                <?php endif; ?>
                            </div>

                            <div class="content-pad">
                                <div class="item-content">
                                    <div class="item-meta blog-item-meta">
                                        <span>By Admin<span class="sep"></span> </span>
                                        <?php $datetime = date_create_from_format('Y-m-d H:i:s', $detail->created_time); ?>
                                        <span><?= date_format($datetime, 'M d, Y') ?> <span class="sep">|</span> </span>
                                    </div>
                                </div>
                            </div>                           
                        </div>
                    </article>
                </div><!-- /col-md-9 -->
            </div>
        </div>
    </div>
    </section>