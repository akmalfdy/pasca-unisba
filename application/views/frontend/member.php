<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Pimpinan dan Dosen</h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row padding-small-v1">
    <div class="container">
        <div class="row">
            <?php foreach($dosen as $key => $row): ?>
            <?php if($key % 3 == 0): ?>
                <?php if($key == 0): ?>
                <div class="col-md-12">
                    <div class="project-listing">
                        <div class="project-portfolio v1">
                <?php else: ?>
                </div>
                </div>
                </div>
                <div class="col-md-12">
                    <div class="project-listing">
                        <div class="project-portfolio v1">
                <?php endif; ?>
            <?php else: ?>
                <?php if ($key == (count($clients) - 1)): ?>
                <div class="col-md-12">
                        <div class="project-listing">
                            <div class="project-portfolio v1">
                <?php endif; ?>
            <?php endif; ?>                       
                        <div class="item j" style="width: 30%;">
                            <div class="thumb-item">
                                <div class="item-thumbnail" style="height:400px;">
                                    <a href="member-single.html"><img style="height:500px;" src="<?= base_url('assets/uploads/img/'.$row->img_url)?>" alt="image"></a>
                                </div><!-- /item-thumbnail -->

                                <div class="item-content">
                                    <h3 class="item-title">
                                        <a href="member-single.html"><?= $row->nama ?></a>
                                    </h3>
                                    <h4 class="small-text"><?= $row->jabatan ?></h4>
                                </div><!-- /item-content -->
                            </div><!-- /thumb-item -->
                        </div><!-- /item -->
            <?php if ($key == (count($clients) - 1)): ?>
            </div>
            </div>
            </div>
            <?php endif; ?>    
            <?php endforeach; ?>
        </div>
    </div>
</section>