<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Biaya Pendidikan</h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row padding-small-v1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size: 18px; padding:10px 0px; color:black;"><?= $biayaMagisterKet->judul ?></h1>
                <div class="table-responsive">          
                    <table class="table table-bordered" style="text-align:center;">
                        
                        <tr style="background-color: #133054;color: white;">
                            <td>SEMESTER</td>
                            <?php $total = []; ?>
                            <?php foreach($biayaMagister[1] as $key => $row): ?>
                                <?php $total[$key] = 0; ?>
                                <td><?= str_replace('Magister', '', (($row->tipe) == 2) ? $row->nama : $row->program_nama) ?></td>
                            <?php endforeach;?>
                        </tr>
                            <?php foreach($biayaMagister as $key => $row): ?>
                            <tr>
                                <td><?= $key?></td>
                                <?php foreach($row as $keyCol => $col): ?>
                                <?php $total[$keyCol] = $total[$keyCol] + $col->harga; ?>
                                <td><?= number_format($col->harga, 0, '.', '.')?></td>
                                <?php endforeach;?>
                            </tr>
                            <?php endforeach;?>
                            <tr>
                                <td><strong>Jumlah</strong></td>
                                <?php foreach($total as $val): ?>
                                <td><strong><?= number_format($val, 0, '.', '.')?></strong></td>
                                <?php endforeach;?>
                            </tr>
                    </table>
                </div>
                <?= $biayaMagisterKet->keterangan ?>
            </div><!-- /col-md-12 -->
        </div>
        <div class="link-center">
            <div class="line-under"></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2 style="font-size: 18px; padding:10px 0px; color:black;"><?= $biayaDoktorKet->judul ?></h2>
                <div class="table-responsive">          
                    <table class="table table-bordered" style="text-align:center;"> 
                        
                        <tr style="background-color: #133054;color: white;">
                            <td>SEMESTER</td>
                            <?php $total = []; ?>
                            <?php foreach($biayaDoktor[1] as $key => $row): ?>
                            <?php $total[$key] = 0; ?>
                                <td><?= str_replace('Magister', '', $row->program_nama) ?></td>
                            <?php endforeach;?>
                        </tr>
                            <?php foreach($biayaDoktor as $key => $row): ?>
                            <tr>
                                <td><?= $key?></td>
                                <?php foreach($row as $keyCol => $col): ?>
                                <?php $total[$keyCol] = $total[$keyCol] + $col->harga; ?>
                                <td><?= number_format($col->harga, 0, '.', '.')?></td>
                                <?php endforeach;?>
                            </tr>
                            <?php endforeach;?>
                            <tr>
                                <td><strong>Jumlah</strong></td>
                                <?php foreach($total as $val): ?>
                                <td><strong><?= number_format($val, 0, '.', '.')?></strong></td>
                                <?php endforeach;?>
                            </tr>
                    </table>
                </div>
                <?= $biayaDoktorKet->keterangan ?>
            </div><!-- /col-md-12 -->
        </div>
    </div>
</section>