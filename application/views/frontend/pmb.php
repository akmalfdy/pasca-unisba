<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Informasi PMB</h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row padding-small-v1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="panel panel-default expandable">
                        <div class="panel-heading clickable collapsed" style="background-color: #e25e0c; color:white;">Magister</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php foreach($magister as $key => $row): ?>
                                    <div class="expandable">
                                        
                                        <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;border-bottom: 1px;border-style: solid;padding: 10px 0px;"><strong> <?= $row->program_nama ?></strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                                        
                                        <div class="content-body">
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>PENDAHULUAN</strong><span class="pull-right"></span></h2>
                                            <hr>
                                            <?= $row->pendahuluan?>
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>KOMPETENSI LULUSAN</strong><span class="pull-right"></span></h2>
                                            <hr>
                                            <?= $row->kompetensi_lulusan?>
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>FASILITAS</strong><span class="pull-right"></i></span></h2>
                                            <hr>
                                            <?= $row->fasilitas?>
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>BIAYA</strong><span class="pull-right"></span></h2>
                                            <hr>
                                            <div class="table-responsive">          
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr style="background-color: #133054;color: white;">
                                                        <th>SEMESTER</th>
                                                        <?php foreach($biayaMagister[1] as $row): ?>
                                                            <th><?= str_replace('Magister', '', $row->program_nama) ?></th>
                                                        <?php endforeach;?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($biayaMagister as $key => $row): ?>
                                                        <tr>
                                                            <td><?= $key?></td>
                                                            <?php foreach($row as $col): ?>
                                                            <td><?= number_format($col->harga, 0, '.', '.')?></td>
                                                            <?php endforeach;?>
                                                        </tr>
                                                        <?php endforeach;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default expandable">
                        <div class="panel-heading clickable collapsed" style="background-color: #e25e0c; color:white;">Doktor</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php foreach($doktor as $key => $row): ?>
                                    <div class="expandable">
                                        
                                        <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;border-bottom: 1px;border-style: solid;padding: 10px 0px;"><strong> <?= $row->program_nama ?></strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                                        
                                        <div class="content-body">
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>PENDAHULUAN</strong><span class="pull-right"></span></h2>
                                            <hr>
                                            <?= $row->pendahuluan?>
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>KOMPETENSI LULUSAN</strong><span class="pull-right"></span></h2>
                                            <hr>
                                            <?= $row->kompetensi_lulusan?>
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>FASILITAS</strong><span class="pull-right"></i></span></h2>
                                            <hr>
                                            <?= $row->fasilitas?>
                                            <hr>
                                            <h2 class="" style="text-align: center; margin: 10px 0px;"><strong>BIAYA</strong><span class="pull-right"></span></h2>
                                            <hr>
                                            <div class="table-responsive">          
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr style="background-color: #133054;color: white;">
                                                        <th>SEMESTER</th>
                                                        <?php foreach($biayaDoktor[1] as $row): ?>
                                                            <th><?= str_replace('Magister', '', $row->program_nama) ?></th>
                                                        <?php endforeach;?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($biayaDoktor as $key => $row): ?>
                                                        <tr>
                                                            <td><?= $key?></td>
                                                            <?php foreach($row as $col): ?>
                                                            <td><?= number_format($col->harga, 0, '.', '.')?></td>
                                                            <?php endforeach;?>
                                                        </tr>
                                                        <?php endforeach;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div> -->

            <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #e25e0c; color:white;">Prodi & Akreditasi</div>
                    <div class="panel-body">
                        
                            <div class="pf-content">
                            <?= $pmb[4]->konten ?>
                            </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #e25e0c; color:white;">Jadwal Penerimaan Mahasiswa Baru</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <?= $pmb[1]->konten ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #e25e0c; color:white;">Syarat Pendaftaran</div>
                    <div class="panel-body">
                        <div class="row">
                        <div class="pf-content">
                        <?= $pmb[0]->konten ?>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default expandable">
                    <div class="panel-heading clickable collapsed" style="background-color: #e25e0c; color:white;">Materi Ujian Saringan Masuk</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <?= $pmb[2]->konten ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default expandable">
                    <div class="panel-heading clickable collapsed" style="background-color: #e25e0c; color:white;">Waktu Perkuliahan</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <?= $pmb[3]->konten ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /col-md-12 -->

        </div>
    </div>
</section>