<div id="preloaderKDZ"></div>
  <div class="sn-site">
  <header class="header sn-header-style-1">
    <div class="header-top">
      <div class="container">
        <div class="header-top-left">
          <aside id="text-2" class="widget widget_text">
            <div class="textwidget"><i class="fa fa-envelope-o"></i><?= $contact->email; ?></div>
          </aside>
          <aside id="text-3" class="widget widget_text">
            <div class="textwidget"><i class="fa fa-mobile"></i><?= $contact->phone; ?></div>
          </aside>
          <aside id="text-4" class="widget widget_text">
            <div class="textwidget"><i ></i></div>
          </aside>
        </div>
      </div>
    </div><a href="#primary-menu"><i class="fa fa-bars"></i></a>
    <div class="container">
      <div class="header-bottom">

        <div class="main-nav-wrapper header-left">
          <div class="header-logo pull-left"><a href="<?= base_url('/'); ?>" title="<?= $brand->brand_name; ?>"><img src="<?= base_url(); ?>/assets/uploads/img/brand/<?= $brand->brand_logo; ?>" style="height: 100px;" alt="logo" class="logo-img"/></a></div>
          <!-- .header-logo-->

          <nav id="primary-menu" class="main-nav">
            <ul class="nav">
              <li class="active menu-single menu-home"><a href="<?= base_url('/'); ?>">Home</a>
              <li class="menu-single"><a href="<?= site_url('page/product'); ?>">Product</a></li>
              <li class="menu-single"><a href="<?= site_url('page/about'); ?>">About Us</a>
              <li class="menu-single"><a href="<?= site_url('page/client'); ?>">Client</a>
              <li class="menu-single"><a href="<?= site_url('page/contact'); ?>">Contact Us</a>
              </li>
            </ul>
          </nav>
          <!-- .header-main-nav-->
        </div>
      </div>
    </div>
        
  </header>
    <section>
      <div class="banner-sub-page" style="background-image: url(<?= base_url(); ?>/assets/uploads/img/brand/<?= $brand->brand_background; ?>);">
        <div class="container">
          <div class="breadcrumbs">
            <div class="breadcrumbs-list">
              <div class="page">Home</div>
              <div class="page">Client</div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section data-js-module="filtering-demo" class="big-demo go-wide">
      <div class="container">
       <h2 class="text-center" style="margin-top: 3rem; margin-bottom: 5rem;">Our Clients</h2>
        <ul class="grid da-thumbs woocommerce columns-3">
        <?php foreach($clients as $key => $client): ?>
                  
          <?php if($key % 3 == 0): ?>
            <?php if($key == 0): ?>
            <div id="1a" class="tab-pane active">
            <?php elseif ($key == (count($clients) - 1)): ?>
            </div>
            <?php else: ?>
            </div>
            <div id="2a" class="tab-pane active">
            <?php endif; ?>
          <?php endif; ?>
          <li data-category="Creative" class="element-item creative product-item-wrap">
            <div class="title-bottom">
              <a href="#"><?= $client->client_name ?></a>
              <!-- <p>Creative, Interface</p> -->
            </div>
            <div class="hover-dir">
              <img src="<?= base_url(); ?>assets/uploads/img/client/<?= get_file_dim($client->client_image, 365, 259); ?>" alt="logo1" />
              <div>
                <div class="in-slider">
                  <div class="in-slider-content">
                    <a href="<?= base_url(); ?>assets/uploads/img/client/<?= get_file_dim($client->client_image, 365, 259); ?>" rel="prettyPhoto[gallery1]">
                      <i class="fa fa-search"></i>
                    </a>
                    <a href="portfolio-vertical-slider.html">
                      <h6><?= $client->client_name ?></h6>
                    </a>
                    <!-- <p>Rectangle</p> -->
                  </div>
                </div>
              </div>
            </div>
          </li>
          <?php endforeach;?>
          
        </ul>
        <div style="margin-bottom: 5rem;" class="pagination-product text-right">
          <?= $pagination; ?>
        </div>
      </div>
    </section>