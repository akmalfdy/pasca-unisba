<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Jurnal</h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row padding-v1">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-9">

                <div class="event-listing event-listing-classic">
                    <article class="post-item row event-classic-item">
                        <div class="col-md-4 col-sm-5">
                            <div class="content-pad">
                                <div class="item-thumbnail">
                                    <a href="#">
                                        <img src="http://pasca.unisba.ac.id/wp-content/uploads/SK-BAN-PT-MM-1-726x1024.jpeg" alt="image">
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">22</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-7">
                            <div class="content-pad">
                                <div class="item-content">
                                    <h3 class="item-title">
                                        <a href="#" class="main-color-1-hover">Prodi Magister Manajemen Mendapatkan Akreditasi Peringkat B</a>
                                    </h3>

                                    <p>Business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds.</p>
                                    <div class="event-time">At 8:00 am</div>
                                    
                                </div>
                                <div class="item-meta">
                                    <a class="flat-button" href="#">DETAILS  <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div><!--/event-listing-->

                <div class="event-listing event-listing-classic">
                    <article class="post-item row event-classic-item">
                        <div class="col-md-4 col-sm-5">
                            <div class="content-pad">
                                <div class="item-thumbnail">
                                    <a href="#">
                                        <img src="http://pasca.unisba.ac.id/wp-content/uploads/2015/03/keputusan.jpg" alt="image">
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">30</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-7">
                            <div class="content-pad">
                                <div class="item-content">
                                    <h3 class="item-title">
                                        <a href="#">Keputusan Tahapan Ujian</a>
                                    </h3>
                                    <p>Keputusan Tahapan Ujian</p>
                                    <div class="event-time">At 7:00 am</div>
                                    
                                </div>
                                <div class="item-meta">
                                    <a class="flat-button" href="#">DETAILS
                                    <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div><!--/event-listing-->

                <div class="event-listing event-listing-classic">
                    <article class="post-item row event-classic-item">
                        <div class="col-md-4 col-sm-5">
                            <div class="content-pad">
                                <div class="item-thumbnail">
                                    <a href="#">
                                        <img src="http://pasca.unisba.ac.id/wp-content/uploads/2014/12/1610838_556851031116468_7864371684465903034_n.jpg" alt="image">
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">20</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-7">
                            <div class="content-pad">
                                <div class="item-content">
                                    <h3 class="item-title">
                                    <a href="#" class="main-color-1-hover">Unisba Kawasan Bebas Rokok</a></h3>
                                    <p>Business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds.</p>
                                    <div class="event-time">At 10:00 am</div>
                                    
                                </div>
                                <div class="item-meta">
                                    <a class="flat-button" href="#">DETAILS
                                    <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div><!--/event-listing-->

                <div class="event-listing event-listing-classic">
                    <article class="post-item row event-classic-item">
                        <div class="col-md-4 col-sm-5">
                            <div class="content-pad">
                                <div class="item-thumbnail">
                                    <a href="#">
                                        <img src="http://pasca.unisba.ac.id/wp-content/uploads/Portrait-Copy-2-819x1024.jpg" alt="image">                                       
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">22</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-7">
                            <div class="content-pad">
                                <div class="item-content">
                                    <h3 class="item-title">
                                    <a href="#" class="main-color-1-hover">PENGUMUMAN KELULUSAN SELEKSI MAHASISWA BARU PROGRAM PASCASARJANA UNISBA GELOMBANG I TAHUN AKADEMIK 2018/2019</a></h3>
                                    <p>But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures.</p>
                                    <div class="event-time">At 1:00 pm</div>
                                    
                                </div>
                                <div class="item-meta">
                                    <a class="flat-button" href="#">DETAILS
                                    <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div><!--/event-listing-->

            </div><!-- /col-md-9 -->
        </div>
    </div>
</section>