<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Kurikulum Pascasarjana</h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row flat-member-single padding-v1">
    <div class="container">
        <div class="row">
            <div class="member-single">
                <div class="col-md-9">
                    <h1 style="text-align: center; font-size: 32px;">Kurikulum Pascasarjana UNISBA</h1>
                    <br>
                    <ol style="list-style-type:decimal;">
                        <li>Kurikulum disusun dalam satuan mata kuliah dengan beban satuan kredit semester (sks).</li>
                        <li>Beban studi program magister dirancang antara 36 – 38 (kecuali untuk Program Studi Magister Psikologi Profesi), sks yang ditawarkan kepada peserta program untuk ditempuh dalam kurun waktu selam 4 semester efektif, dengan toleransi masa studi sesuai dengan peraturan yang berlaku yaitu  10 semester.</li>
                        <li>Struktur kurikulum</li>
                    </ol>

                    <?php foreach($kurikulum as $row): ?>
                    <div class="expandable">
                        <hr>
                        <h2 class="clickable" style="text-align: center; margin: 10px 0px;"><strong>Kurikulum Program Studi <?= $row->program_nama?></strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                        
                        <div class="content-body">
                            <?= $row->detail?>
                        </div>
                        <hr>
                    </div>
                    <?php endforeach;?>

                </div><!-- /col-md-9 -->

                <div class="col-md-3">   

                    <div class="widget widget-courses">
                        <h2 class="widget-title">Informasi</h2>
                        <ul class="recent-posts clearfix">
                            <?php foreach($informasi as $row): ?>
                            <li style="border-bottom: 1px solid #eee; margin-bottom:5px;">
                                <div class="text">
                                    <a href="<?= site_url('informasi/detail/'.$row->informasi_id) ?>"><?= $row->judul ?></a>
                                </div>
                            </li>
                            <?php endforeach;?>
                        </ul><!-- /popular-news clearfix -->
                    </div><!-- /widget-posts -->

                    </div><!-- /col-md-9 -->
                    </div><!-- /col-md-3 -->
            </div>
        </div>
    </div>
</section>