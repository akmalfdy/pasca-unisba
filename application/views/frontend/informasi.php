<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Informasi</h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row padding-v1">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-9">
                <?php foreach($informasi as $row): ?>
                <?php $datetime = date_create_from_format('Y-m-d H:i:s', $row->created_time); ?>
                <div class="event-listing event-listing-classic">
                    <article class="post-item row event-classic-item">
                        <div class="col-md-4 col-sm-5">
                            <div class="content-pad">
                                <div class="item-thumbnail">
                                    <a href="#">
                                        <img src="<?= base_url('assets/uploads/img/'.$row->image)?>" alt="image">
                                        <div class="date-block">
                                            <div class="month"><?= date_format($datetime, 'M') ?></div>
                                            <div class="day"><?= date_format($datetime, 'd') ?></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-7">
                            <div class="content-pad">
                                <div class="item-content">
                                    <h3 class="item-title">
                                        <a href="#" class="main-color-1-hover"><?= $row->judul ?></a>
                                    </h3>

                                    <p><?= substr($row->informasi, 0, 100) ?> ...</p>
                                    <div class="event-time">At <?= date_format($datetime, 'g:i a') ?></div>
                                    
                                </div>
                                <div class="item-meta">
                                    <a class="flat-button" href="<?= site_url('informasi/detail/'.$row->informasi_id) ?>">DETAILS  <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div><!--/event-listing-->
                <?php endforeach;?>
                <?= $pagination; ?>
            </div><!-- /col-md-9 -->
        </div>
    </div>
</section>