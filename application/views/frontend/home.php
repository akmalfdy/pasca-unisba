
        <div class="tp-banner-container v1" style="height: 800px;">
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <?php foreach($banner as $key => $row): ?>
                    <div class="item <?= ($key == 0) ? 'active' : '' ?>">
                        <img class="d-block" style="width:100%; height:800px;" src="<?= base_url('assets/uploads/img/'.$row->url)?>" alt="First slide">
                        <?php if($row->content != '' && $row->title != ''): ?>
                        <div class="carousel-caption d-md-block" style="top:1%; background-color:rgba(255,255,255,.85);right:0%; left:50%;padding: 30px 50px;">
                            <div class="slider_title" style="color:black;">
                                <?= $row->title ?>
                                <a href="<?= site_url('pmb'); ?>" class="btn-lg btn-default" style="background-color: #e25e0c; color:white; float:left">Klik Disini</a>
                                <hr>
                                <br>
                                <?= $row->content ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php endforeach; ?>
                    
                </div>
            </div>
            <a class="left carousel-control carousel-middle" href="#carousel" data-slide="prev">
                <span class="fa fa-arrow-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control carousel-middle" href="#carousel" data-slide="next">
                <span class="fa fa-arrow-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div> <!-- /.tp-banner-container -->

        <section class="flat-row padding-v1">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-9">
                        <?php foreach($informasi as $row): ?>
                        <?php $datetime = date_create_from_format('Y-m-d H:i:s', $row->created_time); ?>
                        <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                            <a href="<?= site_url('informasi/detail/'.$row->informasi_id) ?>">
                                                <img src="<?= base_url('assets/uploads/img/'.$row->image)?>" alt="image">
                                                <div class="date-block">
                                                    <div class="month"><?= date_format($datetime, 'M') ?></div>
                                                    <div class="day"><?= date_format($datetime, 'd') ?></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h3 class="item-title">
                                                <a href="<?= site_url('informasi/detail/'.$row->informasi_id) ?>" class="main-color-1-hover"><?= $row->judul ?></a>
                                            </h3>

                                            <p><?= substr($row->informasi, 0, 100) ?> ...</p>
                                            <div class="event-time">At <?= date_format($datetime, 'g:i a') ?></div>
                                            
                                        </div>
                                        <div class="item-meta">
                                            <a class="flat-button" href="<?= site_url('informasi/detail/'.$row->informasi_id) ?>">DETAILS  <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
                        <?php endforeach;?>
                        <a href="<?= site_url('informasi/index/1') ?>" class="btn btn-default" style="color:#e25e0c; font-size:1rem">Lihat Selengkapnya ..</a>
                    </div><!-- /col-md-9 -->
                </div>
            </div>
        </section>

        <section class="flat-row full-color-v3">
        <h1 style="font-size:2rem; text-align:center;">Galeri</h1>
        </section>
        <div class="page-title parallax parallax1 v1" style="">
            <div class="container">
                <div class="row">
                    <div class="post-wrap">                    
                        <div class="posts-carousel v1">
                            <div class="flat-event">
                                <div class="flat-blog-carousel" data-item="3" data-nav="true" data-dots="false" data-auto="true">
                                <?php foreach($galeri as $key => $row): ?>
                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail" style="height: 300px;">
                                                    <a href="<?= site_url('galeri');?>"><img src="<?= base_url('assets/uploads/img/'.$row->galeri_url)?>" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="#">
                                                                <h4><?= $row->galeri_judul ?></h4>
                                                                <!-- <span class="price yellow">£1999</span> -->
                                                            </a>
                                                        </div>                                
                                                    </div> 
                                                </div><!-- /event-overlay -->
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach;?>
                                </div><!-- /flat-blog-carousel -->
                            </div><!-- /flat-event -->
                        </div><!-- /posts-carousel -->
                    </div><!-- /post-wrap -->
                </div><!-- /row -->
            </div><!-- /container -->
        </div><!-- /page-title -->