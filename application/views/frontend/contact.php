<div id="preloaderKDZ"></div>
    <div class="sn-site">
      <header class="header sn-header-style-1">
        <div class="header-top">
          <div class="container">
            <div class="header-top-left">
              <aside id="text-2" class="widget widget_text">
                <div class="textwidget"><i class="fa fa-envelope-o"></i><?= $contact->email; ?></div>
              </aside>
              <aside id="text-3" class="widget widget_text">
                <div class="textwidget"><i class="fa fa-mobile"></i><?= $contact->phone; ?></div>
              </aside>
              <aside id="text-4" class="widget widget_text">
                <div class="textwidget"><i ></i></div>
              </aside>
            </div>
          </div>
        </div><a href="#primary-menu"><i class="fa fa-bars"></i></a>
        <div class="container">
          <div class="header-bottom">
            
            <div class="main-nav-wrapper header-left">
              <div class="header-logo pull-left"><a href="<?= base_url('/'); ?>" title="<?= $brand->brand_name; ?>"><img src="<?= base_url(); ?>/assets/uploads/img/brand/<?= $brand->brand_logo; ?>" style="height: 100px;" alt="logo" class="logo-img"/></a></div>
              <!-- .header-logo-->

              <nav id="primary-menu" class="main-nav">
                <ul class="nav">
                  <li class="active menu-single menu-home"><a href="<?= base_url('/'); ?>">Home</a>
                  <li class="menu-single"><a href="<?= site_url('page/product'); ?>">Product</a></li>
                  <li class="menu-single"><a href="<?= site_url('page/about'); ?>">About Us</a>
                  <li class="menu-single"><a href="<?= site_url('page/client'); ?>">Client</a>
                  <li class="menu-single"><a href="<?= site_url('page/contact'); ?>">Contact Us</a>
                    
                  </li>
                </ul>
              </nav>
              <!-- .header-main-nav-->
            </div>

            

          </div>
        </div>
        
      </header>
      <div id="example-wrapper">
        <div class="section">
          <div class="banner-sub-page">
            <div class="contact-us-map">
              <div class="map-embed">
                <div class="contact-us-map">
                  <div class="map-embed">
                    <iframe src="https://maps.google.com/maps?q=<?= floatval($contact->lat); ?>,<?= floatval($contact->long); ?>&hl=es;z=14&amp;output=embed" width="600" height="450" allowfullscreen=""></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <section>
          <div class="container">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-10">
                <div class="contact-us-content">
                  <h2>PRACTICAL INFORMATION</h2>
                  <div class="wpb_text_column">
                    <div class="wpb_wrapper">
                      <div class="row-icon-box row">
                        <div class="col-md-4">
                          <div class="icon-box icon-box-style1">
                            <div class="icon-box-left"><i class="fa fa-map-marker color-3f font-25"></i></div>
                            <div class="icon-box-right">
                              <p class="color-3f">VISIT US</p><span class="address">
                              <?= $contact->address; ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="icon-box icon-box-style1">
                            <div class="icon-box-left"><i class="fa fa-phone color-3f font-25"></i></div>
                            <div class="icon-box-right">
                              <p class="color-3f">Phone</p><span>Phone : <?= $contact->phone; ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="icon-box icon-box-style1">
                            <div class="icon-box-left"><i class="fa fa-envelope-o color-3f font-25"></i></div>
                            <div class="icon-box-right">
                              <p class="color-3f">WORK WITH US?</p><span>Email : <?= $contact->email; ?></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wpb_text_column wpb_content_element">
                    <div class="wpb_wrapper">
                        <?= $contact->desc; ?>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </section>