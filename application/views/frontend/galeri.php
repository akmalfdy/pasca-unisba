<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Galeri</h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row padding-v1">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php foreach($kategori as $keyGaleri => $galeri): ?>
                <hr style="border-top: #e25e0c 1px solid;">
                <h1 style="text-align:center; margin: 20px; 0px; font-size:1.5rem;"><?= $galeri[0]->galeri_kategori_nama ?></h1>
                <hr style="border-top: #e25e0c 1px solid;">
                <div class="tp-banner-container v1" style="height: 300px;">
                    <div id="carousel<?= $keyGaleri ?>" class="carousel slide" data-ride="carousel" data-interval="false">
                        <ol class="carousel-indicators">
                            <?php foreach($galeri as $key => $row): ?>
                            <li data-target="#carousel<?= $keyGaleri ?>" data-slide-to="<?= $key ?>" class="<?= ($key == 0) ? 'active' : '' ?>"></li>
                            <?php endforeach; ?>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <?php foreach($galeri as $key => $row): ?>
                                <div class="item <?= ($key == 0) ? 'active' : '' ?>">
                                <a href="<?= base_url('assets/uploads/img/'.$row->galeri_url)?>" data-toggle="lightbox" data-max-height="800">
                                    <img class="d-block" style="height:300px; display: block;margin: 0 auto;" src="<?= base_url('assets/uploads/img/'.$row->galeri_url)?>" class="img-fluid">
                                </a>
                                
                                <div class="carousel-caption" style="top: 0;bottom:unset;">
                                    <h3 style="background-color: gray;margin-left: 10%;margin-right: 10%;"><?= $row->galeri_judul ?></h3>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            
                        </div>
                    </div>
                    <a class="left carousel-control carousel-middle" href="#carousel<?= $keyGaleri ?>" data-slide="prev">
                        <span class="fa fa-arrow-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control carousel-middle" href="#carousel<?= $keyGaleri ?>" data-slide="next">
                        <span class="fa fa-arrow-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div> <!-- /.tp-banner-container -->
                <?php endforeach; ?>
            </div><!-- /col-md-9 -->
            <div class="col-md-3 col-md-offset-3">   

                <div class="widget widget-courses">
                    <h2 class="widget-title">Informasi</h2>
                    <ul class="recent-posts clearfix">
                        <?php foreach($informasi as $row): ?>
                        <li style="border-bottom: 1px solid #eee; margin-bottom:5px;">
                            <div class="text">
                                <a href="<?= site_url('informasi/detail/'.$row->informasi_id) ?>"><?= $row->judul ?></a>
                            </div>
                        </li>
                        <?php endforeach;?>
                    </ul><!-- /popular-news clearfix -->
                </div><!-- /widget-posts -->

                </div><!-- /col-md-9 -->
            </div><!-- /col-md-3 -->
        </div>
    </div>
</section>