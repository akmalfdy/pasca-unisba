<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title"><?= $page->page_title ?></h2>
                </div>
                <div class="breadcrumbs">
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<section class="flat-row padding-small-v1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pf-content">
                <?= $page->page_content ?>
                </div>
            </div><!-- /col-md-12 -->

        </div>
    </div>
</section>