        <footer class="footer full-color" style="position:relative">
            <img src="<?= base_url(); ?>/assets/uploads/footer.jpeg" style="width:100%; position:absolute;-webkit-filter: blur(5px);
  -moz-filter: blur(5px);
  -o-filter: blur(5px);
  -ms-filter: blur(5px);
  filter: blur(5px);">
            <section id="bottom">
                <div class="section-inner">
                    <div class="container">
                        <div class="row normal-sidebar">

                            <div class=" widget divider-widget">
                                <div class=" widget-inner">
                                    <div class="un-heading un-separator">
                                        <div class="un-heading-wrap">
                                            <span class="un-heading-line un-heading-before">
                                                <span></span>
                                            </span>
                                          
                                            <span class="un-heading-line un-heading-after">
                                                <span></span>
                                            </span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3  widget widget-nav-menu">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">Program Magister</h2>
                                    <div class="menu-law-business-container">
                                        <ul id="menu-law-business" class="menu">
                                            <?php foreach($magister as $row): ?>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1280"><a href="<?=site_url('program/magister/'.str_replace(" ","-",strtolower($row->program_nama)))?>">Program <?= $row->program_nama?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-md-3  widget widget-nav-menu">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">Program Doktor</h2>
                                    <div class="menu-engineering-container">
                                        <ul id="menu-engineering" class="menu">
                                            <?php foreach($doktor as $row): ?>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1287"><a href="<?=site_url('program/doktor/'.str_replace(" ","-",strtolower($row->program_nama)))?>">Program <?= $row->program_nama?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>  
                            </div>

                            <div class=" col-md-3  widget widget-nav-menu">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">Tautan Populer</h2>
                                    <div class="menu-higher-education-container">
                                        <ul id="menu-higher-education" class="menu">
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1294">
                                                <a href="https://www.unisba.ac.id">Official Website</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1295">
                                                <a href="http://pasca.unisba.ac.id/elearning/">e-Learning</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1296">
                                                <a href="http://pasca.unisba.ac.id/elibrary/">e-Library</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1297">
                                                <a href="#">E-Jurnal</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-md-3 border widget widget-text">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">CONTACT</h2>
                                    <div class="textwidget">
                                        <p>Email: <?= $email ?><br>Telephone: <?= $telp ?><br>
                                        <?= $address ?> </p>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class=" col-md-3  widget divider-widget">
                                <div class=" widget-inner">
                                    <div class="un-heading un-separator">
                                        <div class="un-heading-wrap">
                                            <span class="un-heading-line un-heading-before"><span>
                                                </span></span>
                                            <span class="un-heading-line un-heading-after">
                                                <span></span>
                                            </span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div> -->

                            <!-- <div class=" col-md-3  widget widget-text">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">UNIVERSITY THEME</h2>
                                    <div class="textwidget">University is an advanced Wordpress theme for education, school, training center. There are course and event custom post types so you can easily create and manage course, events. You can also create store using WooCommerce with this theme.
                                    </div>
                                </div>
                            </div>

                            <div class=" col-md-3  widget widget-recent-entries">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">RECENT POSTS</h2>
                                    <ul>
                                        <li>
                                            <a href="#">Your Career Starts Here</a>
                                        </li>
                                        <li>
                                            <a href="#">Spark Of Genius</a>
                                        </li>
                                        <li>
                                            <a href="#">University Ranking</a>
                                        </li>
                                        <li>
                                            <a href="#">Our New Campus</a>
                                        </li>
                                        <li>
                                            <a href="#">Education Organization Theme</a>
                                        </li>
                                        <li>
                                            <a href="#">Summer Is Coming</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class=" col-md-3  widget widget-nav-menu">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">OTHER LINKS</h2>
                                    <div class="menu-others-container">
                                        <ul id="menu-others" class="menu">
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1306">
                                                <a href="#">Our Campus</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1307"><a href="#">Research</a></li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1308"><a href="#">Projects</a></li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1309"><a href="#">Professors</a></li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1310"><a href="#">Job Opportunities</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-md-3  widget widget-flickr">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">Flickr Widget</h2>
                                    <ul class="clearfix">
                                        <li class="last">
                                            <div class="thumb images-hover">
                                                <div class="overlay"></div>
                                                <a href="#">
                                                    <span><img src="images/flickr/1.jpg" alt="image"></span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="last">
                                            <div class="thumb images-hover">
                                                <div class="overlay"></div>
                                                <a href="#">
                                                    <span><img src="images/flickr/2.jpg" alt="image"></span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="last">
                                            <div class="thumb images-hover">
                                                <div class="overlay"></div>
                                                <a href="#">
                                                    <span><img src="images/flickr/3.jpg" alt="image"></span>
                                                </a>
                                            </div>
                                        </li>                            
                                        <li>
                                            <div class="thumb images-hover images-hover">
                                                <div class="overlay"></div>
                                                <a href="#">
                                                    <span><img src="images/flickr/4.jpg" alt="image"></span>
                                                </a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb images-hover">
                                                <div class="overlay"></div>
                                                <a href="#">
                                                    <span><img src="images/flickr/5.jpg" alt="image"></span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="last">
                                            <div class="thumb images-hover">
                                                <div class="overlay"></div>
                                                <a href="#">
                                                    <span><img src="images/flickr/6.jpg" alt="image"></span>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </section>

            <div id="bottom-nav">
                <div class="container">
                    <div class="link-center">
                        <div class="line-under"></div>
                        <a class="flat-button go-top-v1 style1" href="#top">TOP</a>
                    </div>
                    <div class="row footer-content">
                        <div class="copyright col-md-6">
                            <?= $copyright ?>
                        </div>
                        <nav class="col-md-6 footer-social">
                            <ul class="social-list">
                                <li>
                                    <a href="<?= $facebook ?>" class="btn btn-default social-icon">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $twitter ?>" class="btn btn-default social-icon">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div><!--/row-->
                </div><!--/container-->
            </div>
        </footer>

        <!-- Javascript -->
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery.easing.js"></script>
        
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/owl.carousel.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/parallax.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery.tweet.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery-validate.js"></script> 
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery-waypoints.js"></script>

        <!-- Revolution Slider -->
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/slider.js"></script>
        
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/university/javascript/main.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
        
        <script>
          $(document).on('click', '.clickable', function(e){
              var $this = $(this);
              if(!$this.hasClass('collapsed')) {
                  $this.next().slideUp();
                  $this.addClass('collapsed');
                  $this.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
              } else {
                  $this.next().slideDown();
                  $this.removeClass('collapsed');
                  $this.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
              }
          });
          $(document).ready(function(){
            $('.collapsed').parents('.expandable').find('.content-body').slideUp()
          });
          $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({ wrapping: false });
        });
        </script>
    </div>
</body>
</html>