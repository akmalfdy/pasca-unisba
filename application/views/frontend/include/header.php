<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>UNISBA - Pascasarjana</title>

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/university/stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/university/stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/university/stylesheets/responsive.css">
    
    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/university/stylesheets/colors/color1.css" id="colors">
    
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/university/stylesheets/animate.css">
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    
    <!-- Favicon and touch icons  -->
    <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="#" rel="shortcut icon">

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->

</head>

<body class="header-sticky">

    <div class="boxed">

        <div class="menu-hover">
            <div class="btn-menu">
                <span></span>
            </div><!-- //mobile menu button -->
        </div>

        <div class="header-inner-pages">
            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar menu-top">
                                <ul class="menu"> 
                                <li class="home">
                                        <a href="http://www.unisba.ac.id">UNISBA</a>
                                    </li>
                                    <li class="home">
                                        <a href="<?= base_url(); ?>">Beranda</a>
                                    </li>
                                    <li class="home">
                                        <a href="<?=site_url('galeri')?>">Galeri</a>
                                    </li>
                                    <li class="home">
                                        <a href="<?=site_url('LayananSarana')?>">Layanan dan Sarana</a>
                                    </li>
                                    <li class="home">
                                        <a href="http://pasca.unisba.ac.id/elearning/">e-Learning</a>
                                    </li>
                                    <li class="home">
                                        <a href="http://pasca.unisba.ac.id/elibrary/">e-Library</a>
                                    </li>
                                    <li class="home">
                                        <a href="#">E-Jurnal</a>
                                    </li>
                                </ul><!-- /.menu -->
                            </nav><!-- /.mainnav -->

                            <!-- <a class="navbar-right search-toggle show-search" href="#">
                                <i class="fa fa-search"></i>
                            </a> -->
                            
                            <!-- <div class="submenu top-search">
                                <form class="search-form">
                                    <div class="input-group">
                                        <input type="search" class="search-field" placeholder="Search Here">
                                        <span class="input-group-btn">
                                            <button type="submit"><i class="fa fa-search fa-4x"></i></button>
                                        </span>
                                    </div>
                                </form>
                            </div> -->

                            <div class="navbar-right topnav-sidebar">
                                <ul class="textwidget">
                                    <li>
                                        <a href="<?= $facebook ?>"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="<?= $twitter ?>"><i class="fa fa-twitter"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- col-md-12 -->
                    </div><!-- row -->
                </div><!-- container -->
            </div><!-- Top -->    
        </div><!-- header-inner-pages -->

        <!-- Header -->
        <header id="header" class="header">
            <div class="header-wrap">
                <div class="container">
                    <div class="header-wrap clearfix">
                        <div id="logo" class="logo">
                            <a href="<?= base_url(); ?>" rel="home">
                                <img src="<?= base_url(); ?>/assets/university/images/logo-univ.png" style="height:64px;" alt="image">
                            </a>
                        </div><!-- /.logo -->


                        <div class="nav-wrap">

                            <nav id="mainnav" class="mainnav">
                                <ul class="menu"> 
                                    <li class="home">
                                        <a href="<?= base_url(); ?>">Tentang Kami</a>
                                        <ul class="submenu">
                                            <li><a href="<?=site_url('page/member')?>">Pimpinan dan Dosen</a></li>
                                            <li><a href="<?=site_url('page/struktur-organisasi')?>">Struktur Organisasi</a></li>
                                            <li><a href="<?=site_url('page/sejarah')?>">Sejarah Pascasarjana</a></li>
                                            <li><a href="<?=site_url('page/visi-misi')?>">Visi Misi</a></li>
                                            <li><a href="<?=site_url('page/tujuan')?>">Tujuan Kami</a></li>
                                        </ul><!-- /.submenu -->
                                    </li>
                                    <li>
                                        <a href="#">Program Magister</a>
                                        <ul class="submenu" style="width: 400px;">
                                            <?php foreach($magister as $row): ?>
                                            <li><a href="<?=site_url('program/magister/'.str_replace(" ","-",strtolower($row->program_nama)))?>">Program <?= $row->program_nama?></a></li>
                                            <?php endforeach; ?>
                                        </ul><!-- /.submenu -->
                                    </li> 
                                    <li>
                                        <a href="#">Program Doktor</a>
                                        <ul class="submenu" style="width: 400px;">
                                            <?php foreach($doktor as $row): ?>
                                            <li><a href="<?=site_url('program/doktor/'.str_replace(" ","-",strtolower($row->program_nama)))?>">Program <?= $row->program_nama?></a></li>
                                            <?php endforeach; ?>
                                        </ul><!-- /.submenu -->
                                    </li>    
                                    <li>
                                        <a href="<?=site_url('page/kalender')?>">Kalender Akademik & Kurikulum</a>
                                    </li>
                                    <li>
                                        <a href="<?=site_url('biaya')?>">Biaya Kuliah</a>
                                    </li> 
                                    <li>
                                        <a href="<?=site_url('pmb')?>">Informasi PMB</a>
                                    </li>                                    
                                </ul><!-- /.menu -->
                            </nav><!-- /.mainnav -->
                        </div><!-- /.nav-wrap -->
                    </div><!-- /.header-wrap -->
                </div><!-- /.container-->
            </div><!-- /.header-wrap-->
        </header><!-- /.header -->