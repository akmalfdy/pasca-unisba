<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title"><?= $program->program_nama?></h2>
                </div>
                <div class="breadcrumbs">
                    <ul>
                        <li> <?= $program->program_nama?></li>
                    </ul>                   
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->

<!-- Blog posts -->
<section class="flat-row flat-member-single padding-v1">
            <div class="container">
                <div class="row">
                    <div class="member-single">
                        <div class="col-md-9">
                            <div class="member-single-post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="item-thumbnail">
                                            <a href="#"><img src="<?= base_url('assets/uploads/img/'.$program->img_url)?>" alt="image"></a>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title"><?= $program->nama?></h3>
                                                <h4 class="small-text">Ketua Prodi</h4>
                                                <strong>Konsentrasi</strong> :
                                                <?= $program->konsentrasi?>
                                            </div>
                                        </div><!--/content-pad-->
                                    </div><!--/col-md-8-->
                                </div><!--/row-->
                            </div><!--/member-single-post-->  
                            
                            <br>
                            <p><strong>Program Pascasarjana (S2)</strong><br>
                            <strong> Universitas Islam Bandung</strong><br>
                            <strong> Jl. Purnawarman No. 59 Telp 022-4203368 Ext. 148-149 Fax. 4219134 Bandung &nbsp;40116</strong><br>
                            <strong> Email:&nbsp; <a href="mailto:pascasarjana@unisba.ac.id">pascasarjana@unisba.ac.id</a></strong></p>
                            <br>
                            <div class="expandable">
                                <hr>
                                <h2 class="clickable" style="text-align: center; margin: 10px 0px;"><strong>PENDAHULUAN</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                
                                <div class="content-body">
                                    <p style="text-align: justify;">Berdasarkan keputusan direktur Direktorat Jenderal Pendidikan tinggi republik indonesia nomor 235/Dikti/kep/1998 tanggal 09 Juli 1998. Universitas Islam Bandung menyelenggarakan program magister (S2) Ilmu hukum dengan konsentrasi hukum islam, hukum pidana, hukum administrasi negara dengan kekhususan otonomi daerah, dan hukum bisnis,</p>
                                    <p style="text-align: justify;">Dokter dan dokter gigi sebagai profesi yang mulia, bebas dan penuh pengabdian. Namun demikian, saat ini mulai adanya pengaruh hukum yang cukup penting terhadap keberlangsungan keleluasaan profesi dokter dan dokter gigi memahami hukum, paling tidak mengetahui mana yang sesuai hukum dan mana yang melanggar hukum dalam menjalankan profesinya.</p>
                                    <p style="text-align: justify;">Berdasarkan hal itu, Tahun akademik 2006/2007 program studi ilmu hukum Pascasarjana UNISBA membuka konsentrasi hukum kesehatan.</p>
                                    <p style="text-align: justify;">Sehubungan dengan perkembangan dan kebutuhan masyarakat dan konsekuensi Indonesia sebagai anggota WTO, maka diperlukan ahli -ahli Hukum Internasional yang menguasai hukum internasional publik maupun privat maka tahun 2012 Pascasarjana Unisba membuka Konsentrasi Hukum Internasional.</p>
                                    <p style="text-align: justify;">Pada tahun 2015 telah Terakreditasi A-843/BAN-PT/Akred/M/VIII/2015.</p>
                                </div>
                                <hr>
                            </div>

                            <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong>KOMPETENSI LULUSAN</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
    
                                <div class="content-body">
                                    <?= $program->kompetensi_lulusan?>
                                </div>
                                <hr>
                            </div>

                            <!-- <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong> PENGELOLA</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                            
                                <div class="content-body">
                                    <?= $program->pengelola?>
                                </div>
                                <hr>
                            </div>

                            <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong>PENERIMAAN MAHASISWA BARU</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                               
                                <div class="content-body">
                                    <?= $program->pmb?>
                                </div>
                                <hr>
                            </div>

                            <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong>Kontak Person</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                            
                                <div class="content-body">
                                    <?= $program->kontak_person?>
                                </div>
                                <hr>
                            </div>
 -->
                            <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong>FASILITAS</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                                
                                <div class="content-body">
                                    <?= $program->fasilitas?>
                                </div>
                                <hr>
                            </div>
                            <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong>PROSES BELAJAR</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                                
                                <div class="content-body">
                                    <?= $program->proses_belajar?>
                                </div>
                                <hr>
                                
                            </div>

                            <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong>TAHAPAN UJIAN</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                               
                                <div class="content-body">
                                    <?= $program->tahapan_ujian?>
                                </div>
                                <hr>
                            </div>

                            <div class="expandable">
                                <hr>
                                <h2 class="clickable collapsed" style="text-align: center; margin: 10px 0px;"><strong>DOSEN PENGAJAR</strong><span class="pull-right"><i class="fa fa-chevron-up"></i></span></h2>
                               
                                <div class="content-body">
                                    <?= $program->pengajar?>
                                </div>
                                <hr>
                            </div>
                            
                            
                        </div><!-- /col-md-9 -->

                        <div class="col-md-3">   

                            <div class="widget widget-courses">
                                <h2 class="widget-title">Informasi</h2>
                                <ul class="recent-posts clearfix">
                                    <?php foreach($informasi as $row): ?>
                                    <li style="border-bottom: 1px solid #eee; margin-bottom:5px;">
                                        <div class="text">
                                            <a href="<?= site_url('informasi/detail/'.$row->informasi_id) ?>"><?= $row->judul ?></a>
                                        </div>
                                    </li>
                                    <?php endforeach;?>
                                </ul><!-- /popular-news clearfix -->
                            </div><!-- /widget-posts -->

                            </div><!-- /col-md-9 -->
                            </div><!-- /col-md-3 -->
                    </div>
                </div>
            </div>
        </section>
        