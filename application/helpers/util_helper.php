<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function load_view($nav, $subnav, $data) {
    $ci = & get_instance();
    $ci->load->model('Contact_model');
    $data['nav'] = $nav;
    $data['subnav'] = $subnav;
    $data['copyright'] = $ci->Contact_model->item('copyright')->contact_value;

    $ci->load->view('include/header', $data);
    $ci->load->view('example.php', $data);
    $ci->load->view('include/footer', $data);
}

function view_front($view, $data) {
    $ci = & get_instance();
    $ci->load->model([
        'Page_model', 'Contact_model', 'Member_model', 'Program_model', 'Kalender_model', 'Informasi_model'
    ]);

    $data['address'] = $ci->Contact_model->item('address')->contact_value;
    $data['magister'] = $ci->Program_model->getMagister();
    $data['doktor'] = $ci->Program_model->getDoktor();
    $data['email'] = $ci->Contact_model->item('email')->contact_value;
    $data['telp'] = $ci->Contact_model->item('telp')->contact_value;
    $data['facebook'] = $ci->Contact_model->item('facebook')->contact_value;
    $data['twitter'] = $ci->Contact_model->item('twitter')->contact_value;
    $data['copyright'] = $ci->Contact_model->item('copyright')->contact_value;

    $ci->load->view('frontend/include/header', $data);
    $ci->load->view('frontend/'.$view, $data);
    $ci->load->view('frontend/include/footer', $data);
}

function get_file_dim($filename, $width, $height){
    return substr_replace($filename, '-'.$width.'x'.$height, strrpos($filename, '.'), 0);
}
