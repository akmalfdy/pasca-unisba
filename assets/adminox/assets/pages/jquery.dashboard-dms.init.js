
/**
* Theme: Adminox Admin Template
* Author: Coderthemes
* Morris Chart
*/

!function($) {
    "use strict";

    var MorrisCharts = function() {};
    var GoogleChart = function() {
        this.$body = $("body")
    };

    //creates Stacked chart
    MorrisCharts.prototype.createStackedChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            stacked: true,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barColors: lineColors
        });
    },

    //creates pie chart
    GoogleChart.prototype.createPieChart = function(selector, data, colors, is3D, issliced) {
        var options = {
            fontName: 'Open Sans',
            fontSize: 13,
            height: 300,
            width: 500,
            pieSliceText: 'value-and-percentage',
            chartArea: {
                left: 50,
                width: '90%',
                height: '90%'
            },
            colors: colors
        };

        if(is3D) {
            options['is3D'] = true;
        }

        if(issliced) {
            options['is3D'] = true;
            options['pieSliceText'] = 'value-and-percentage';
            options['slices'] = {
                2: {offset: 0.15},
                5: {offset: 0.1}
            };
        }

        var google_chart_data = google.visualization.arrayToDataTable(data);
        var pie_chart = new google.visualization.PieChart(selector);
        pie_chart.draw(google_chart_data, options);
        return pie_chart;
    },
    
    MorrisCharts.prototype.init = function() {

        //creating Stacked chart
        var $stckedData  = [
            { y: '2005', a: 45, b: 180, c: 100 },
            { y: '2006', a: 75,  b: 65, c: 80 },
            { y: '2007', a: 100, b: 90, c: 56 },
            { y: '2008', a: 75,  b: 65, c: 89 },
            { y: '2009', a: 100, b: 90, c: 120 },
            { y: '2010', a: 75,  b: 65, c: 110 },
            { y: '2011', a: 50,  b: 40, c: 85 },
            { y: '2012', a: 75,  b: 65, c: 52 },
            { y: '2013', a: 50,  b: 40, c: 77 },
            { y: '2014', a: 75,  b: 65, c: 90 },
            { y: '2015', a: 100, b: 90, c: 130 },
            { y: '2016', a: 80, b: 65, c: 95 }
        ];
        this.createStackedChart('morris-bar-stacked-1', $stckedData, 'y', ['a', 'b', 'c'], ['Completed', 'In Progress','Waiting'], ['#4489e4', '#78c350','#f96a74']);

        //creating Stacked chart
        var $stckedData  = [
            { y: '2005', a: 45, b: 180, c: 100 },
            { y: '2006', a: 75,  b: 65, c: 80 },
            { y: '2007', a: 100, b: 90, c: 56 },
            { y: '2008', a: 75,  b: 65, c: 89 },
            { y: '2009', a: 100, b: 90, c: 120 },
            { y: '2010', a: 75,  b: 65, c: 110 },
            { y: '2011', a: 50,  b: 40, c: 85 },
            { y: '2012', a: 75,  b: 65, c: 52 },
            { y: '2013', a: 50,  b: 40, c: 77 },
            { y: '2014', a: 75,  b: 65, c: 90 },
            { y: '2015', a: 100, b: 90, c: 130 },
            { y: '2016', a: 80, b: 65, c: 95 }
        ];
        this.createStackedChart('morris-bar-stacked-2', $stckedData, 'y', ['a', 'b', 'c'], ['Completed', 'In Progress','Waiting'], ['#4489e4', '#78c350','#f96a74']);

        //creating Stacked chart
        var $stckedData  = [
            { y: '2005', a: 45, b: 180, c: 100 },
            { y: '2006', a: 75,  b: 65, c: 80 },
            { y: '2007', a: 100, b: 90, c: 56 },
            { y: '2008', a: 75,  b: 65, c: 89 },
            { y: '2009', a: 100, b: 90, c: 120 },
            { y: '2010', a: 75,  b: 65, c: 110 },
            { y: '2011', a: 50,  b: 40, c: 85 },
            { y: '2012', a: 75,  b: 65, c: 52 },
            { y: '2013', a: 50,  b: 40, c: 77 },
            { y: '2014', a: 75,  b: 65, c: 90 },
            { y: '2015', a: 100, b: 90, c: 130 },
            { y: '2016', a: 80, b: 65, c: 95 }
        ];
        this.createStackedChart('morris-bar-stacked-3', $stckedData, 'y', ['a', 'b', 'c'], ['Completed', 'In Progress','Waiting'], ['#4489e4', '#78c350','#f96a74']);

        //creating Stacked chart
        var $stckedData  = [
            { y: '2005', a: 45, b: 180, c: 100 },
            { y: '2006', a: 75,  b: 65, c: 80 },
            { y: '2007', a: 100, b: 90, c: 56 },
            { y: '2008', a: 75,  b: 65, c: 89 },
            { y: '2009', a: 100, b: 90, c: 120 },
            { y: '2010', a: 75,  b: 65, c: 110 },
            { y: '2011', a: 50,  b: 40, c: 85 },
            { y: '2012', a: 75,  b: 65, c: 52 },
            { y: '2013', a: 50,  b: 40, c: 77 },
            { y: '2014', a: 75,  b: 65, c: 90 },
            { y: '2015', a: 100, b: 90, c: 130 },
            { y: '2016', a: 80, b: 65, c: 95 }
        ];
        this.createStackedChart('morris-bar-stacked-4', $stckedData, 'y', ['a', 'b', 'c'], ['Completed', 'In Progress','Waiting'], ['#4489e4', '#78c350','#f96a74']);

    },

    //init
    GoogleChart.prototype.init = function () {
        var $this = this;


        //creating pie chart
        var pie_data = [
            ['Task', 'Hours per Day'],
            ['Work',     11],
            ['Eat',      2],
            ['Commute',  2],
            ['Watch TV', 2],
            ['Sleep',    7]
        ];

        //creating 3d pie chart
        $this.createPieChart($('#pie-3d-chart-1')[0], pie_data, ['#5553ce','#297ef6', '#e52b4c', '#ffa91c', '#32c861'], true, false);
        $this.createPieChart($('#pie-3d-chart-2')[0], pie_data, ['#5553ce','#297ef6', '#e52b4c', '#ffa91c', '#32c861'], true, false);
        $this.createPieChart($('#pie-3d-chart-3')[0], pie_data, ['#5553ce','#297ef6', '#e52b4c', '#ffa91c', '#32c861'], true, false);
        $this.createPieChart($('#pie-3d-chart-4')[0], pie_data, ['#5553ce','#297ef6', '#e52b4c', '#ffa91c', '#32c861'], true, false);

        //on window resize - redrawing all charts
        $(window).on('resize', function() {
            $this.createPieChart($('#pie-3d-chart-1')[0], pie_data, ['#188ae2', '#4bd396', '#f9c851', '#f5707a', '#6b5fb5'], true, false);
            $this.createPieChart($('#pie-3d-chart-2')[0], pie_data, ['#188ae2', '#4bd396', '#f9c851', '#f5707a', '#6b5fb5'], true, false);
            $this.createPieChart($('#pie-3d-chart-3')[0], pie_data, ['#188ae2', '#4bd396', '#f9c851', '#f5707a', '#6b5fb5'], true, false);
            $this.createPieChart($('#pie-3d-chart-4')[0], pie_data, ['#188ae2', '#4bd396', '#f9c851', '#f5707a', '#6b5fb5'], true, false);
        });
    },
    //init GoogleChart
    $.GoogleChart = new GoogleChart, $.GoogleChart.Constructor = GoogleChart
    
    //init
    $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.MorrisCharts.init();
    //loading visualization lib - don't forget to include this
    google.load("visualization", "1", {packages:["corechart"]});
    //after finished load, calling init method
    google.setOnLoadCallback(function() {$.GoogleChart.init();});
}(window.jQuery);

/**
 * Theme: Adminox Admin Template
 * Author: Coderthemes
 * Module/App: Flot-Chart
 */

! function($) {
    "use strict";

    

    

    
    
}(window.jQuery),

//initializing GoogleChart
function($) {
    "use strict";
    
}(window.jQuery);
